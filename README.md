### API Base
library/config.js
live -> export const localUrl = "https://api.nourishwithsim.org/api/";
dev -> export const localUrl = "https://nwsapi.underdev.in/api/";
test -> export const localUrl = "https://testnwsapi.underdev.in/api/";

### Pusher Base
library/config.js
dev & test -> export const pusherKey = "d7d4a056535c53ef539e";
live -> export const pusherKey = "a00ba68339c8d955e76f";

### Braches
Main
### Build
1. Move the build of dev, live, test to respective folder in nws-nutritionist-build and push the code to repo.
    Example: 
        If you have changes to be pushed in DEV
        Run build with dev config
        Delete all the files in nws-nutritionist-build/dev
        Move the build files to nws-nutritionist-build/dev
        Push the code to repo and then follow next step
2. Open nws-server-dev folder in git-bash and follow server-key text file instructions
3. For Live server only, initimate backend team