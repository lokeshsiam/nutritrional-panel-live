import React from 'react';
import "../App.css";
import Close from "../assets/images/closeimg.png";
import Img1 from "../assets/images/image1.png";
import ViewDetails from "../pages/Viewdetails";


export default function Dietplan() {
    return (

        <>
            {/* <ViewDetails /> */}

            <div className='main-body'>
                <div className='main-content'>
                    <b className='diet-plan'>Diet Plan</b>

                    <div className='sub-heading'>
                        <b>diet: Vegan</b>
                        <b>plan name: program 2 vegan</b>
                        <b>week: 7 week</b>
                    </div>

                    <div class="dropdown button-one">
                        <button class="btn btn-light dropdown-toggle " type="button" data-toggle="dropdown">PT W1
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu ">
                            <li><a href="#">HTML</a></li>
                            <li><a href="#">CSS</a></li>
                            <li><a href="#">JavaScript</a></li>
                        </ul>
                    </div>

                    <div className='small-box'>
                        <div className='small-box-sub-navigation'>
                            <p>On Walking</p>
                            <p>pre breakfast</p>
                            <p>breakfast</p>
                            <p>morning snack</p>
                            <p>lunch</p>
                            <p>evening snack</p>
                            <p>dinner</p>
                            <p>post dinner</p>
                        </div>
                        <div className='inner-small-box'>
                            <div className='inner-image-box'>
                                <img className="img_one" src={Img1} />
                            </div>

                            <p className='inner-small-box-txt-1'>Dish Name</p>
                            <p className='inner-small-box-1'>Oats</p>

                            <p className='inner-small-box-txt-2'>Dish Type</p>
                            <p className='inner-small-box-1'>Optionalss</p>

                            <p className='inner-small-box-txt-3'>Discription</p>
                            <p className='inner-small-box-3'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

                            <p className='inner-small-box-txt-4'>Replacement/others</p>
                            <p className='inner-small-box-4'></p>
                        </div>


                    </div>
                </div>



            </div>


        </>


    );
}
