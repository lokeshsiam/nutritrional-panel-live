import React, { useState, useEffect } from "react";
import { Button, Form, Col, Row, Tabs, Tab } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";
import Pusher from "pusher-js";
import { useHistory } from "react-router-dom";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import moment, { weekdaysShort } from "moment";
import _ from "lodash";

import {
  getMethod,
  postMethod,
  getTableData,
  getTableDataNoLoad,
  tokenError,
  tableDefault,
} from "../Library";

import ModalPop from "../components/modals/big";

import Table from "../components/tables/table";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComments, faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { InputGroup, FormControl } from "react-bootstrap";
import { Line } from "react-chartjs-3";
import PlaceholderImage from "../assets/images/placeholder.png";

import { Bar } from "react-chartjs-3";
import GalleryBox from "../components/GalleryBox";
import { pusherKey } from "../Library/configs";

export function LineChart(props) {
  return (
    <div>
      {/* <h5 className="mt-3">
        <u>{props.data.current_day_name}</u>
      </h5> */}
      <Line
        data={props.data}
        width={null}
        height={null}
        options={props.options}
      />
    </div>
  );
}

export function ProgressTrackerBody(props) {
  const [formInputs, setFormInputs] = useState();
  const [key, setKey] = useState("1");
  const [loaded, setLoaded] = useState(false);

  // const [searchValue, setSearchValue] = useState('');
  
  const [options, setOptions] = useState({
    responsive: true,
    legend: {
      display: true,
      position: "bottom",
    },
    scales: {
      yAxes: {
        type: "linear",
        display: true,
        title: {
          display: true,
          text: "y axis",
        },
        ticks: {
          suggestedMin: 35,
          suggestedMax: 200,
        },
      },
      xAxes: [
        {
          ticks: {
            stepSize: 1,
            beginAtZero: true,
            autoSkip: false,
            maxRotation: 0,
            minRotation: 0,
          },
        },
      ],
    },
    type: "line",
  });

  // gallerybox
  const [photos, setPhotos] = useState([]);
  const [captions, setCaptions] = useState([]);

  // gallerybox

  const getInfo = async () => {
    const data = {
      url: "view_progress_summary_by_user_id",
      body: {
        user_id: props.user_id[0],
        date: props.user_id[1],
      },
    };
    const newData = await postMethod(data);
    // setFormInputs(newData.data); // temp test
    setLoaded(true);
    if (newData.status == 1) {
      setFormInputs(newData.data);
      updatePhotos(newData.data.before_after_images);
    } else if (newData.status == 0) {
      setFormInputs();
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  const updatePhotos = (data) => {
    let phts = [];
    let cpts = [];
    data.forEach((item) => {
      let op = [];
      if (item?.after_image) {
        op.push(item?.after_image);
      }
      if (item?.before_Image) {
        op.push(item?.before_Image);
      }
      phts.push(...op);
      cpts.push(`${item.view} - (After)`, `${item.view} - (Before)`);
    });
    // console.log("photo ", phts, cpts);
    setPhotos(phts);
    setCaptions(cpts);
  };

  useEffect(() => {
    getInfo();
    return () => {
      setLoaded(false);
      setFormInputs();
    };
  }, []);

  // console.log(formInputs.measurement)

  return (
    <>
      {loaded ? (
        formInputs ? (
          <>
            {/* <Row className="mb-4">
              <Col sm="6">
                <Form.Label>User Name</Form.Label>
                <Form.Control
                  type="text"
                  value={formInputs.user_name}
                  plaintext
                  readOnly
                />
              </Col>
              <Col sm="6">
                <Form.Label>Current stage</Form.Label>
                <Form.Control
                  type="text"
                  value={
                    formInputs.progress_tracker
                      ? formInputs.progress_tracker.workout_week
                      : "N/A"
                  }
                  plaintext
                  readOnly
                />
              </Col>
            </Row> */}
            <Row>
              <Col>
                <h5 className="mb-4">
                  {/* <u></u> */}
                  Progress Tracker -{" "}
                  <span className="text-capitalize">
                    {formInputs?.measurement?.current_day_name
                      ? formInputs?.measurement?.current_day_name
                      : "Week"}
                  </span>
                </h5>
              </Col>
            </Row>

            <Row>
              <Col style={{ minHeight: "425px" }}>
                <Tabs
                  id="progress-tracker-tab"
                  activeKey={key}
                  onSelect={(k) => setKey(k)}
                >
                  <Tab eventKey="1" title="Measurements">
                    <LineChart
                      data={formInputs.measurement}
                      options={options}
                    />
                  </Tab>
                  <Tab eventKey="2" title="Weights">
                    <LineChart data={formInputs.weight} options={options} /> 
                  </Tab>
                </Tabs>
              </Col>
            </Row>

            <Row>
              {/* <Col> */}
              {/* <Row className="dateBox">
                  <Col>
                    <InputGroup className="d-flex align-items-center">
                      <Form.Control
                        type="text"
                        value={
                          formInputs.first_date ? formInputs.first_date : "N/A"
                        }
                        plaintext
                        readOnly
                      />
                      <InputGroup.Append>
                        <FontAwesomeIcon icon={faCalendarAlt} />
                      </InputGroup.Append>
                    </InputGroup>
                  </Col>
                  <Col>
                    <InputGroup className="d-flex align-items-center">
                      <Form.Control
                        type="text"
                        value={
                          formInputs.last_date ? formInputs.last_date : "N/A"
                        }
                        plaintext
                        readOnly
                      />
                      <InputGroup.Append>
                        <FontAwesomeIcon icon={faCalendarAlt} />
                      </InputGroup.Append>
                    </InputGroup>
                  </Col>
                </Row>
                 */}
              {/* <Row className="mt-5 mb-4 prgImg"> */}
              <Col sm="12" className="mt-5">
                <Row style={{ paddingRight: "15px" }}>
                  {formInputs.before_after_images.map((item, index) => (
                    <Col
                      className="d-flex no-gutter flex-column bfAfImgBox"
                      key={index}
                    >
                      <div className="collImage row no-gutter">
                        <Col className="pr-0 position-relative">
                          <img
                            src={
                              item?.before_Image
                                ? item.before_Image
                                : PlaceholderImage
                            }
                            // onClick={openLightbox}
                            width="100%"
                          />
                          <label>Before</label>
                        </Col>
                        <Col className="px-0 position-relative">
                          <img
                            src={
                              item?.after_image
                                ? item.after_image
                                : PlaceholderImage
                            }
                            width="100%"
                            // onClick={openLightbox}
                          />
                          <label>After</label>
                        </Col>
                      </div>
                      <label className="text-center mt-0 mb-3">
                        {item.view}
                      </label>
                    </Col>
                  ))}
                </Row>
              </Col>
              {/* </Row> */}
              {/* </Col> */}
              {/* <Col></Col> */}
            </Row>

            {/* <Row className="mt-3 mb-4">
              <Col sm="8">
                <Form.Label>Current weight</Form.Label>
                <Form.Control
                  type="text"
                  value={formInputs.weight ? formInputs.weight : "N/A"}
                  plaintext
                  readOnly
                />
              </Col>
            </Row> */}

            {/* <Form.Group controlId="notes">
              <Form.Label>User Notes</Form.Label>
              <Form.Control
                as="textarea"
                value={
                  formInputs.progress_tracker &&
                  formInputs.progress_tracker.how_do_you_feel
                    ? formInputs.progress_tracker.how_do_you_feel
                    : "N/A"
                }
                plaintext
                readOnly
                rows={3}
              />
            </Form.Group>
           */}

            {/* photo viewer */}
            <GalleryBox images={photos} captions={captions} position="center" />
          </>
        ) : (
          <p>
            Oops!! There is no search result for this user based on the
            criteria.
          </p>
        )
      ) : (
        <p>Loading...</p>
      )}
    </>
  );
}

export default function ManageUserProgress() {
  const [modalShow, setModalShow] = useState(false);
  const [offset, setOffset] = useState(1);
  const [perPage] = useState(10);
  const [pageCount, setPageCount] = useState(0);
  const [tableList, setTableList] = useState([]);
  const [programList, setProgramList] = useState([]);
  const [userId, setUserId] = useState();
  const [dateError, setDateError] = useState(false);
  const [pusherChannel, setPusherChannel] = useState();
  const [searchValue, setSearchValue] = useState("");
  const [pusherName, setPusherName] = useState();
  const [totalCount, setTotalCount] = useState();
  const [filter, setFilter] = useState(false);
  const [filterActivity, setFilterActivity] = useState([]);
  const [filterVarious, setFilterVarious] = useState([]);
  const [week, setWeek] = useState("");
  const [filterId, setFilterId] = useState([])
  
 

  const [filterInputs, setFilterInputs] = useState({
    program_id: "",
    from_date: "",
    to_date: "",
    by_activity:"",
    various_program:"",
    program_week:""
  });

  const [dateInputs, setDateInputs] = useState({
    select_to_date: null,
    select_from_date: null,
  });

  // const history = useHistory();

  useEffect(() => {
    if (dateInputs.select_from_date && dateInputs.select_to_date) {
      if (
        new Date(dateInputs.select_from_date).getTime() <=
        new Date(dateInputs.select_to_date).getTime()
      ) {
        setDateError(false);
      } else {
        setDateError(true);
      }
    } else {
      setDateError(false);
    }
  }, [dateInputs, setDateInputs]);

  // useEffect(() => {
  //   getTableList('list_user_progress')
  // }, [dateInputs, offset]);

  const clearDate = () => {
    setDateInputs({
      select_to_date: null,
      select_from_date: null,
    });
    setFilterInputs({
      program_id: "",
      from_date: "",
      to_date: "",
      various_program:"",
      by_activity:"",
      program_week:""
    });
    setDateError(false);
    setOffset(1);
    // console.log(dateInputs,filterInputs)
   setTimeout(() => {
     setOffset(1);
    clearFn();
   }, 500); 
  };
  const clearFn =()=>{
    getTableList('list_user_progress',true,1)
  }
  const getTableList = async (url,a,b) => {
    var result;
    if (a==true){
      const filters = {
        program_id:  null,
        by_activity:null,
        various_program:null,
        current_week:null,
        from_date:  null,
        to_date: null,
      };
      var result = _.pickBy(filters, (v) => !_.isUndefined(v) && !_.isNull(v));
  
    }else{
    const filters = {
      program_id: filterInputs.program_id ? filterInputs.program_id : null,
      by_activity:filterInputs.by_activity?filterInputs.by_activity:null,
      various_program:filterInputs.various_program?filterInputs.various_program:null,
      current_week:filterInputs.program_week?filterInputs.program_week:null,
      from_date: filterInputs.from_date ? filterInputs.from_date : null,
      to_date: filterInputs.to_date ? filterInputs.to_date : null,
    };
    var result = _.pickBy(filters, (v) => !_.isUndefined(v) && !_.isNull(v));
  }
    const data = {
      url: "list_user_progress",
      body: {
        search: { value: searchValue },
        length: 10,
        start:b?0: offset === 0 ? 0 : (offset - 1) * 10,
        draw: 10,
        ...result,
      },
    };
    let tableList = await postMethod(data);

    // console.log("tablelist", tableList.user_list);
    if (tableList) {
      setTableList(tableList.data.user_list);
      setTotalCount(tableList);
      setPageCount(Math.ceil(tableList.data.recordsTotal / perPage));
     setFilterId(tableList.data.filtered_users);

    } else {
      setTableList([]);
    }
  };

  const handleSearchInput = (e) => {
    setSearchValue(e.target.value);

    setOffset(1);
  };
  const getTableListNoLoad = async (url) => {
    let tableList = await getTableDataNoLoad(url);
    // console.log("tablelist", tableList.user_list);
    if (tableList && tableList.user_list) {
      setTableList(tableList.user_list);
    }
  };

  const handleFilterInput = async (e) => {
    setFilterInputs((prev) => ({
      ...prev,
      [e.target.name]: e.target.value ? e.target.value : null,
    }));
  };

  const handleFilterSubmit = async (e) => {
    setOffset(1)
    e.preventDefault();
    const filters = {
      program_id: filterInputs.program_id ? filterInputs.program_id : null,
      by_activity:filterInputs.by_activity?filterInputs.by_activity:null,
      various_program:filterInputs.various_program?filterInputs.various_program:null,
      current_week:filterInputs.program_week?filterInputs.program_week:null,
      from_date: filterInputs.from_date ? filterInputs.from_date : null,
      to_date: filterInputs.to_date ? filterInputs.to_date : null,
    };

    let result = _.pickBy(filters, (v) => !_.isUndefined(v) && !_.isNull(v));

    const data = {
      url: "list_user_progress",
      body: {
        search: { value: searchValue },
        length: 10,
        start: offset == (0 || 1) ? 0 : (offset - 1) * 10,
        draw: 10,
        ...result,
      },
    };
    const newData = await postMethod(data);
    if (newData.status == 1) {
      setTableList(newData.data.user_list);
      // console.log(newData.data,'list')
      setPageCount(Math.ceil(newData.data.recordsTotal / perPage));
     setFilterId(newData.data.filtered_users);
  


    } else if (newData.status === false) {
      // console.log('elseif')
      tokenError(newData.error);
    } else {
      setTableList([]);
    }
  };
  // useEffect(() => {
  //   getTableList("list_user_progress");
  // }, [offset])

  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    setOffset(selectedPage + 1);
  };

  const minTwoDigit = (n) => {
    return (n < 10 ? "0" : "") + n;
  };

  const formatDates = (date) => {
    const day = minTwoDigit(date.getDate());
    const month = minTwoDigit(date.getMonth() + 1);
    const year = date.getFullYear();
    let val = `${year}-${month}-${day}`;
    return val;
  };

  const selectUploadDt = (dateSel, name, dataid) => {
    if (dateSel) {
      const date = new Date(`${dateSel}`);
      const chkDt = date.getDate();
      if (chkDt > 0) {
        let val = formatDates(date);
        setFilterInputs((prev) => ({
          ...prev,
          [name]: val,
        }));

        setDateInputs((prev) => ({
          ...prev,
          [dataid]: dateSel,
        }));
      }
    } else {
      setFilterInputs((prev) => ({
        ...prev,
        [name]: null,
      }));

      setDateInputs((prev) => ({
        ...prev,
        [dataid]: null,
      }));
    }
  };

  const getProgramList = async () => {
    const data = {
      url: "master_program_name_list",
    };
    const newData = await getMethod(data);
    if (newData.status == 1) {
      setProgramList(newData.data.master_programs_name_list);
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };
  const getWeek = async () => {
    const data = {
      url: "static_content_list",
    };
    const newData = await getMethod(data);
    if (newData.status == 1) {
    setWeek(newData.data.four_week_with_program_weeks)
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  const getFilterList = async () => {
    const data = {
      url: "get_filter_options",
    };
    const newData = await getMethod(data);
    if (newData.status == 1) {
     console.log(newData.data,'data')
     setFilterActivity(newData.data.by_activity)
     setFilterVarious(newData.data.various_filter)
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  }

  // const clearDate = () => {
  //   setDateInputs({
  //     select_to_date: null,
  //     select_from_date: null,
  //   });
  //   setFilterInputs({
  //     program_id: "",
  //     from_date: "",
  //     to_date: "",
  //   });
  //   getTableList("list_user_progress");
  //   setDateError(false);
  // };

  const initPusher = () => {
    const id = localStorage.getItem("user");
    const UserId = id ? JSON.parse(id).id : "";
    const pusher = new Pusher(pusherKey, {
      cluster: "ap2",
      encrypted: true,
    });
    const channel = pusher.subscribe(`ch-${UserId}`);
    // console.log("pusher unsubsribed loop init");
    setPusherChannel(channel);
    setPusherName(pusher);
    channel.bind("refersh-api", (data) => {
      if (data.refresh_list_nutritions_user_name == 1) {
        getTableListNoLoad("list_user_progress");
      }
    });
  };

  useEffect(() => {
    getTableList("list_user_progress");
    getProgramList();
    initPusher();
    getFilterList();
    getWeek();

  }, [offset, searchValue]);

  // useEffect(() => {
  //   console.log("pusher unsubsribed loop", pusherName, pusherChannel);
  // }, [pusherName, pusherChannel, setPusherChannel, setPusherName]);

  const pusherUnsubsribe = () => {
    if (pusherName && pusherChannel) {
      pusherName.unsubscribe(pusherChannel);
      pusherChannel.unbind();
      // console.log("pusher unsubsribed main user progress");
      // history.push({
      //   pathname: `/user-chat/`,
      //   key: `user-chat-${row.id}`,
      //   state: {
      //     user_id: row.id,
      //     user_image_url: row.image_url,
      //     user_name: row.first_name,
      //   },
      // });
    }
  };

  useEffect(() => {
    localStorage.setItem('filterIds', filterId)
  }, [filterId])

  const tableFilters = () => {
    return <> </>;
  };

  function chatTimeFormatter(cell, row) {
    return (
      <span className="text-capitalize">
        {row?.last_chat_date ? moment(row.last_chat_date).format("LLL") : "-"}
      </span>
    );
  }
  
  const btnFormatter=(cell, row,id,formatExtraData)=> {
    var id = totalCount;
    console.log(formatExtraData,'sffffffffff')
     return (
       <>
         <Link
           variant="link"
           className="position-relative userChatCount "
           onClick={() => pusherUnsubsribe()}
           to={{
             pathname: `/user-chat/`,
             key: `user-chat-${row.id}`,
             state: {
               user_id: row.id,
               user_image_url: row.image_url,
               user_name: row.first_name,
               ids:formatExtraData
             },
           }}
         >
           <FontAwesomeIcon icon={faComments} />
           <span className={row.chat_historys_count > 0 ? "count" : "nocount"}>
             {row.chat_historys_count}
           </span>
         </Link>
       </>
     );
   }

  // function progFormatter(cell, row) {
  //   return (
  //     <>
  //       <Button
  //         variant="link"
  //         onClick={() => {
  //           setModalShow(true);
  //           setUserId(row.id);
  //         }}
  //       >
  //         <u>View</u>
  //       </Button>
  //     </>
  //   );
  // }

  // function mealFormatter(cell, row) {
  //   return (
  //     <>
  //       <Link
  //         variant="link"
  //         to={{
  //           pathname: `/meal-tracker/`,
  //           key: `meal-tracker-${row.id}`,
  //           state: {
  //             user_id: row.id,
  //             user_image_url: row.image_url,
  //             user_name: row.first_name,
  //           },
  //         }}
  //       >
  //         <u>View</u>
  //       </Link>
  //     </>
  //   );
  // }

  const columns = [
    { dataField: "sno", text: "S.No" },
    { dataField: "first_name", text: "User Name" },
    // { dataField: "email", text: "Email ID" },
    // { dataField: "mobile", text: "Mobile Number" },
    { dataField: "onboarded_date", text: "Onboarded Date" },
    { dataField: "program_name", text: "Program Name" },
    // { dataField: "current_week", text: "Current Week" },
    // { dataField: "workout_progress", text: "Workout Progress" },
    // {
    //   dataField: "program_weekly",
    //   text: "Progress Weekly",
    //   formatter: progFormatter,
    // },
    // {
    //   dataField: "meal_tracker",
    //   text: "Meal Tracker",
    //   formatter: mealFormatter,
    // },
    { dataField: "las", text: "Last Chat", formatter: chatTimeFormatter },
    { dataField: "id", text: "Action", formatExtraData: filterId,formatter: btnFormatter },
  ];

  const modalBody = () => {
    return <ProgressTrackerBody user_id={userId} />;
  };

  return (
    <main>
      <section className="rightContent">
        <h3>Manage User Progress</h3>
        <Form className="inline-form mt-5" onSubmit={handleFilterSubmit}>
          <Form.Row className="align-items-center">
            <Col md={4} className="d-flex align-items-center mt-2 mb-2">
              <Form.Label className="mr-3">Program Name</Form.Label>
              <Form.Control
                as="select"
                name="program_id"
                value={filterInputs.program_id}
                onChange={handleFilterInput}
              >
                <option value="">Select a Program</option>
                {programList &&
                  programList.map((item, index) => (
                    <option value={item.id} key={index}>
                      {item.program_name}
                    </option>
                  ))}
              </Form.Control>
            </Col>
            <Col md={4} className="d-flex align-items-center mt-2 mb-2">
              <Form.Label className="mr-3">Onboarded Date</Form.Label>
              <Row className="datePickerBox position-relative">
                <Col className="pr-0">
                  <DatePicker
                    onChange={(date) =>
                      selectUploadDt(date, "from_date", "select_from_date")
                    }
                    selected={
                      dateInputs.select_from_date
                        ? dateInputs.select_from_date
                        : null
                    }
                    maxDate={new Date()}
                    name="from_date"
                    dataid="select_from_date"
                    dateFormat="yyyy/MM/dd"
                    placeholderText="yyyy/MM/dd"
                    autoComplete="off"
                  />
                </Col>
                <Col className="pr-0">
                  <DatePicker
                    onChange={(date) =>
                      selectUploadDt(date, "to_date", "select_to_date")
                    }
                    selected={
                      dateInputs.select_to_date
                        ? dateInputs.select_to_date
                        : null
                    }
                    name="to_date"
                    dataid="select_to_date"
                    maxDate={new Date()}
                    dateFormat="yyyy/MM/dd"
                    placeholderText="yyyy/MM/dd"
                    autoComplete="off"
                  />
                </Col>
                {dateError && (
                  <small className="text-center mt-1 mb-2 fromToErr text-capitalize err-feedback">
                    Invalid From/To Date
                  </small>
                )}
              </Row>
            </Col>

            <Col md={3} className="d-flex align-items-center ml-2 mt-2 mb-2">
              <Form.Label>By Activity</Form.Label>
              <Form.Control
                as="select"
                name="by_activity"
                value={filterInputs.by_activity}
                onChange={handleFilterInput}
              >
                <option value="">Select an Activity</option>
                {filterActivity &&
                  filterActivity.map((item, index) => (
                    <option value={item} key={index}>
                      {item}
                    </option>
                  ))}
              </Form.Control>
            </Col>
          </Form.Row>
          <Form.Row className="align-items-center">
            <Col md={4}className="d-flex align-items-center mt-2 mb-2">
              <Form.Label className="mr-3 filter_right"> Filter</Form.Label>
              <Form.Control
                as="select"
                name="various_program"
                value={filterInputs.various_program}
                onChange={handleFilterInput}
              >
                <option value="">Select a Filter</option>
                {filterVarious &&
                  filterVarious.map((item, index) => (
                    <option value={item} key={index}>
                      {item}
                    </option>
                  ))}
              </Form.Control>
            </Col>

            <Col md={4}className="d-flex align-items-center mt-2 mb-2">
              <Form.Label className="mr-3 filter_right">Weeks</Form.Label>
              <Form.Control
                as="select"
                name="program_week"
                value={filterInputs.program_week}
                onChange={handleFilterInput}
              >
                <option value="">Select a Week</option>
                {week &&
                  week.map((item, index) => (
                    <option value={item} key={index}>
                      {item}
                    </option>
                  ))}
              </Form.Control>
            </Col>
          </Form.Row>



          <Form.Row className="align-items-center justify-content-center mt-4">
            <Col xs="auto">
              <Button
                type="reset"
                variant="secondary"
                className="mb-2 mr-3"
                disabled={
                  dateInputs.select_to_date ||
                  dateInputs.select_from_date ||
                  filterInputs.program_id ||
                  filterInputs.by_activity ||
                  filterInputs.program_week ||
                  filterInputs.various_program
                    ? false
                    : true
                }
                onClick={clearDate}
              >
                Clear
              </Button>
              <Button
                type="submit"
                className="mb-2"
                disabled={
                  (dateInputs.select_to_date ||
                    dateInputs.select_from_date ||
                    filterInputs.program_id ||
                    filterInputs.by_activity ||
                    filterInputs.program_week ||
                    filterInputs.various_program) &&
                  dateError == false
                    ? false
                    : true
                }
              >
                Apply Filter
              </Button>
            </Col>
          </Form.Row>
        </Form>
        <div className="d-flex">
          <InputGroup className="search-register-user">
            <FormControl
              placeholder="Search"
              aria-label="search"
              aria-describedby="basic-addon1"
              className="search-regUser-input"
              onChange={handleSearchInput}
            />
          </InputGroup>
        </div>
        <Table
          data={tableList}
          offset={offset}
          columns={columns}
          filters={tableFilters}
          searchBar="false"
        />
        <div className="d-flex float-right justify-content-end">
          <ReactPaginate
            previousLabel={"prev"}
            nextLabel={"next"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            forcePage={offset - 1}
            onPageChange={handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </div>

        <ModalPop
          show={modalShow}
          modalSize="big"
          onHide={() => {
            setModalShow(false);
          }}
          modalcontent={modalBody()}
          modalhead="Progress Tracker"
        />
      </section>
    </main>
  );
}
