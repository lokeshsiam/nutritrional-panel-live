import React, { useEffect, useState, useRef } from "react";
import "../App.css";
import { Link, Switch } from "react-router-dom";
import Close from "../assets/images/closeimg.png";
import Img1 from "../assets/images/image1.png";
import Modal from 'react-modal'
import { Button, Form, Col, Row, Tabs, Tab } from "react-bootstrap";
import { LineChart } from "./manageuserprogress";
import { Bar } from "react-chartjs-3";
import Pfimg1 from "../assets/images/profileimg1.png";
import Dietplan from "./Dietplan";

import {
    BrowserRouter as Router,
    // Switch,
    Route,
    withRouter,
    useLocation,
} from "react-router-dom";

export default function ViewDetails(props) {

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [formInputs, setFormInputs] = useState();
    const [key, setKey] = useState("1");




    //Line chart

    const [options, setOptions] = useState({
        responsive: true,
        legend: {
            display: true,
            position: "bottom",
        },
        scales: {
            yAxes: {
                type: "linear",
                display: true,
                title: {
                    display: true,
                    text: "y axis",
                },
                ticks: {
                    suggestedMin: 35,
                    suggestedMax: 200,
                },
            },
            xAxes: [
                {
                    ticks: {
                        stepSize: 1,
                        beginAtZero: true,
                        autoSkip: false,
                        maxRotation: 0,
                        minRotation: 0,
                    },
                },
            ],
        },
        type: "line",
    });


    const measurments = {
        labels: ['PT W1', 'PT W2', 'W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7'],
        data: [10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60],
        datasets: [
            {
                label: 'cheast',
                fill: false,
                data: [1, 2, 3, 4, 5, 6, 7, 8, 90],
                borderColor: ['red'],
                borderWidth: 1,
                pointBackgroundColor: "red",
            },
            {
                label: 'waist',
                fill: false,
                data: [4, 5, 6, 7, 8,],
                borderColor: ['blue'],
                borderWidth: 1,
                pointBackgroundColor: "blue",
            },
            {
                label: 'arm',
                fill: false,
                lineThickness: 10,
                data: [6, 7, 8, 9, 10,],
                borderColor: ['yellow'],
                borderWidth: 1,
                pointBackgroundColor: "yellow",
            },
            {
                label: 'leg',
                fill: false,
                data: [25, 30, 15],
                borderColor: ['green'],
                borderWidth: 1,
                pointBackgroundColor: "green",
            },
            {
                label: 'hip',
                fill: false,
                data: [10, 25, 5],
                borderColor: ['pink'],
                borderWidth: 1,
                pointBackgroundColor: "pink",
            }

        ]
    }

    const weight = {
        labels: ['PT W1', 'PT W2', 'W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7'],
        data: [45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95],
        datasets: [
            {
                label: 'weight',
                fill: false,
                data: [75, 80, 85, 90],
                borderColor: ['blue'],
                borderWidth: 1,
                pointBackgroundColor: "blue",
            }

        ]
    }

    //bar chart

    // const [options2, setOptions2] = useState({
    //     responsive: true,
    //     legend: {
    //         display: true,
    //         position: "bottom",
    //     },
    //     scales: {
    //         yAxes: {
    //             type: "linear",
    //             display: true,
    //             title: {
    //                 display: true,
    //                 text: "y axis",
    //             },
    //             ticks: {
    //                 suggestedMin: 35,
    //                 suggestedMax: 200,
    //             },
    //         },
    //         xAxes: [
    //             {
    //                 ticks: {
    //                     stepSize: 1,
    //                     beginAtZero: true,
    //                     autoSkip: false,
    //                     maxRotation: 0,
    //                     minRotation: 0,
    //                 },
    //             },
    //         ],
    //     },
    //     type: "bar",
    // });

    // const mealtracker = {
    //     labels: ['PT W1', 'PT W2', 'W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7'],
    //     data: ["dinner", "evening"],
    //     datasets: [
    //         {
    //             label: 'cheast',
    //             fill: false,
    //             data: [1, 2, 3, 4, 5, 6, 7, 8, 90],
    //             borderColor: ['red'],
    //             borderWidth: 1,
    //             pointBackgroundColor: "red",
    //         },
    //         {
    //             label: 'waist',
    //             fill: false,
    //             data: [4, 5, 6, 7, 8,],
    //             borderColor: ['blue'],
    //             borderWidth: 1,
    //             pointBackgroundColor: "blue",
    //         },
    //         {
    //             label: 'arm',
    //             fill: false,
    //             lineThickness: 10,
    //             data: [6, 7, 8, 9, 10,],
    //             borderColor: ['yellow'],
    //             borderWidth: 1,
    //             pointBackgroundColor: "yellow",
    //         },
    //         {
    //             label: 'leg',
    //             fill: false,
    //             data: [25, 30, 15],
    //             borderColor: ['green'],
    //             borderWidth: 1,
    //             pointBackgroundColor: "green",
    //         },
    //         {
    //             label: 'hip',
    //             fill: false,
    //             data: [10, 25, 5],
    //             borderColor: ['pink'],
    //             borderWidth: 1,
    //             pointBackgroundColor: "pink",
    //         }

    //     ]
    // }

    return (

        <>

            <Modal isOpen={true} >
                <Row>
                    <Col className="modal-close">
                        <div style={{ width: "65%" }} >
                            <Tabs
                                id="progress-tracker-tab"
                                activeKey={key}
                                onSelect={(k) => setKey(k)}
                            >
                                <Tab eventKey="1" title="Water Intake" >
                                    <b >Water Intake</b>
                                </Tab>
                                <Tab eventKey="2" title="Steps Count">
                                    <b>Steps Count</b>
                                </Tab>
                                <Tab eventKey="3" title="Workout %">
                                    <b>Workout %</b>
                                </Tab>
                                <Tab eventKey="4" title="Diet Plan">
                                    {/* <Dietplan /> */}
                                    <div className='main-body'>
                                        <div className='main-content'>
                                            <b className='diet-plan'>Diet Plan</b>
                                            <div className='sub-heading'>
                                                <b>diet: Vegan</b>
                                                <b>plan name: program 2 vegan</b>
                                                <b>week: 7 week</b>
                                            </div>

                                            <div class="dropdown button-one">
                                                <button class="btn btn-light dropdown-toggle " type="button" data-toggle="dropdown">
                                                    PT W1
                                                </button>
                                                <ul class="dropdown-menu ">
                                                    <li><a href="#">HTML</a></li>
                                                    <li><a href="#">CSS</a></li>
                                                    <li><a href="#">JavaScript</a></li>
                                                </ul>
                                            </div>

                                            <div className='small-box'>
                                                <div className='small-box-sub-navigation'>
                                                    <p>On Walking</p>
                                                    <p>pre breakfast</p>
                                                    <p>breakfast</p>
                                                    <p>morning snack</p>
                                                    <p>lunch</p>
                                                    <p>evening snack</p>
                                                    <p>dinner</p>
                                                    <p>post dinner</p>
                                                </div>
                                                <div className='inner-small-box'>
                                                    <div className='inner-image-box'>
                                                        <img className="img_one" src={Img1} />
                                                    </div>

                                                    <p className='inner-small-box-txt-1'>Dish Name</p>
                                                    <p className='inner-small-box-1'>Oats</p>

                                                    <p className='inner-small-box-txt-2'>Dish Type</p>
                                                    <p className='inner-small-box-1'>Optionalss</p>

                                                    <p className='inner-small-box-txt-3'>Discription</p>
                                                    <p className='inner-small-box-3'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

                                                    <p className='inner-small-box-txt-4'>Replacement/others</p>
                                                    <p className='inner-small-box-4'></p>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </Tab>
                                <Tab eventKey="5" title="Progress tracker">
                                    <div className="main-body-pt">
                                        {/* <ViewDetails /> */}
                                        <div className="main-content">
                                            <p className="nav-one-heading">
                                                <b>process tracker-PT Week 2- Day 5</b>
                                            </p>
                                            <div class="dropdown button-one">
                                                <button class="btn btn-light dropdown-toggle " type="button" data-toggle="dropdown">PT W1
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu ">
                                                    <li><a href="#">HTML</a></li>
                                                    <li><a href="#">CSS</a></li>
                                                    <li><a href="#">JavaScript</a></li>
                                                </ul>
                                            </div>

                                            <div className="main-chart">
                                                <div className="chart-one">
                                                    <Tabs>
                                                        <Tab eventKey="1" title="Measurements">
                                                            <LineChart data={measurments} options={options} />
                                                        </Tab>
                                                        <Tab eventKey="2" title="Weights">
                                                            <LineChart data={weight} options={options} />
                                                        </Tab>
                                                    </Tabs>
                                                </div>
                                            </div>

                                            <div className="six-profile-img">
                                                <div className="set-1">
                                                    <div className="fv-box-1">
                                                        <img className="profile_img_one" src={Pfimg1} />
                                                    </div>
                                                    <div className="fv-box-1">
                                                        <img className="profile_img_one" src={Pfimg1} />
                                                    </div>
                                                    {/* <div className="view">
                                                    <p>Front View</p>
                                                </div> */}
                                                </div>

                                                <div className="set-2">
                                                    <div className="fv-box-2">
                                                        <img className="profile_img_one" src={Pfimg1} />
                                                    </div>
                                                    <div className="fv-box-2">
                                                        <img className="profile_img_one" src={Pfimg1} />
                                                    </div>
                                                    {/* <div className="view">
                                                    <p>Back View</p>
                                                </div> */}
                                                </div>

                                                <div className="set-3">
                                                    <div className="fv-box-3">
                                                        <img className="profile_img_one" src={Pfimg1} />
                                                    </div>
                                                    <div className="fv-box-3">
                                                        <img className="profile_img_one" src={Pfimg1} />
                                                    </div>
                                                    {/* <div className="view">
                                                        <p>Side View</p>
                                                    </div> */}
                                                </div>
                                            </div>

                                            <div className="open-img-gallery">
                                                <button type="button" classbtn btn-link>
                                                    <u>
                                                        <b>Open image gallery</b>
                                                    </u>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </Tab>
                                <Tab eventKey="6" title="Meal tracker">
                                    <b>Meal tracker</b>

                                    {/* <div className="main-chart">LineChart
                                                <div className="chart-one">
                                                    <Tabs>
                                                        <Tab eventKey="1" title="Measurements">
                                                            <BarChart data={measurments} options={options} />
                                                        </Tab>
                                                        <Tab eventKey="2" title="Weights">
                                                            <Chart data={weight} options={options} />
                                                        </Tab>
                                                    </Tabs>
                                                </div>
                                            </div> */}

                                </Tab>

                            </Tabs>
                        </div>

                        <div>
                            <img className="img_chat_box_close" src={Close} onClick={() => props.close(false)} />
                        </div>
                    </Col>
                </Row>

            </Modal>


        </>

    );
}
