import React, { useState, useEffect } from "react";
import ViewDetails from "../pages/Viewdetails";
import { Line } from "react-chartjs-3";
// import Linechart from "../pages/Linechart";
import { LineChart } from "./manageuserprogress";
import Pfimg1 from "../assets/images/profileimg1.png";
import { Button, Form, Col, Row, Tabs, Tab } from "react-bootstrap";


function Processtracker() {

  // const [formInputs, setFormInputs] = useState();
  // const [key, setKey] = useState("1");


  // const [options, setOptions] = useState({
  //   responsive: true,
  //   legend: {
  //     display: true,
  //     position: "bottom",
  //   },
  //   scales: {
  //     yAxes: {
  //       type: "linear",
  //       display: true,
  //       title: {
  //         display: true,
  //         text: "y axis",
  //       },
  //       ticks: {
  //         suggestedMin: 35,
  //         suggestedMax: 200,
  //       },
  //     },
  //     xAxes: [
  //       {
  //         ticks: {
  //           stepSize: 1,
  //           beginAtZero: true,
  //           autoSkip: false,
  //           maxRotation: 0,
  //           minRotation: 0,
  //         },
  //       },
  //     ],
  //   },
  //   type: "line",
  // });


  // const measurments = {
  //   labels: ['PT W1', 'PT W2', 'W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7'],
  //   data: [10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60],
  //   datasets: [
  //     {
  //       label: 'cheast',
  //       fill: false,
  //       data: [1, 2, 3, 4, 5, 6, 7, 8, 90],
  //       borderColor: ['red'],
  //       borderWidth: 1,
  //       pointBackgroundColor: "red",
  //     },
  //     {
  //       label: 'waist',
  //       fill: false,
  //       data: [4, 5, 6, 7, 8,],
  //       borderColor: ['blue'],
  //       borderWidth: 1,
  //       pointBackgroundColor: "blue",
  //     },
  //     {
  //       label: 'arm',
  //       fill: false,
  //       lineThickness: 10,
  //       data: [6, 7, 8, 9, 10,],
  //       borderColor: ['yellow'],
  //       borderWidth: 1,
  //       pointBackgroundColor: "yellow",
  //     },
  //     {
  //       label: 'leg',
  //       fill: false,
  //       data: [25, 30, 15],
  //       borderColor: ['green'],
  //       borderWidth: 1,
  //       pointBackgroundColor: "green",
  //     },
  //     {
  //       label: 'hip',
  //       fill: false,
  //       data: [10, 25, 5],
  //       borderColor: ['pink'],
  //       borderWidth: 1,
  //       pointBackgroundColor: "pink",
  //     }

  //   ]
  // }

  // const weight = {
  //   labels: ['PT W1', 'PT W2', 'W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7'],
  //   data: [45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95],
  //   datasets: [
  //     {
  //       label: 'weight',
  //       fill: false,
  //       data: [75, 80, 85, 90],
  //       borderColor: ['blue'],
  //       borderWidth: 1,
  //       pointBackgroundColor: "blue",
  //     }

  //   ]
  // }


  return (
    <>
      <div className="main-body-pt">
        {/* <ViewDetails /> */}
        <div className="main-content">
          <p className="nav-one-heading">
            <b>process tracker-PT Week 2- Day 5</b>
          </p>
          <div class="dropdown button-one">
            <button class="btn btn-light dropdown-toggle " type="button" data-toggle="dropdown">PT W1
              <span class="caret"></span></button>
            <ul class="dropdown-menu ">
              <li><a href="#">HTML</a></li>
              <li><a href="#">CSS</a></li>
              <li><a href="#">JavaScript</a></li>
            </ul>
          </div>

          {/* <div className="two-nav-btn">
            <div className="measure">
              <Tabs
                id="progress-tracker-tab"
                activeKey={key}
                onSelect={(k) => setKey(k)}
              >
                <Tab eventKey="1" title="Measurements">
                </Tab>

                <Tab eventKey="2" title="Weights">
                </Tab>
              </Tabs>
            </div>
            <div className="wgt">
            </div>
          </div> */}

          <div className="main-chart">
            <div className="chart-one">
              {/* <LineChart 
              data={data}
              options={options}
              /> */}

              {/* <Row>
                <Col style={{ Height: "425px" }}>
                  <Tabs
                    id="progress-tracker-tab"
                    activeKey={key}
                    onSelect={(k) => setKey(k)}
                  >
                    <Tab eventKey="1" title="Measurements">
                      <LineChart data={measurments} options={options} />
                    </Tab>
                    <Tab eventKey="2" title="Weights">
                      <LineChart data={weight} options={options} />
                    </Tab>
                  </Tabs>
                </Col>
              </Row> */}

            </div>
          </div>

          <div className="six-profile-img">
            <div className="set-1">
              <div className="fv-box-1">
                <img className="profile_img_one" src={Pfimg1} />
              </div>
              <div className="fv-box-1">
                <img className="profile_img_one" src={Pfimg1} />
              </div>
              {/* <div className="view">
                <p>Front View</p>
              </div> */}
            </div>

            <div className="set-2">
              <div className="fv-box-2">
                <img className="profile_img_one" src={Pfimg1} />
              </div>
              <div className="fv-box-2">
                <img className="profile_img_one" src={Pfimg1} />
              </div>
              {/* <div className="view">
                <p>Back View</p>
              </div> */}
            </div>

            <div className="set-3">
              <div className="fv-box-3">
                <img className="profile_img_one" src={Pfimg1} />
              </div>
              <div className="fv-box-3">
                <img className="profile_img_one" src={Pfimg1} />
              </div>
              {/* <div className="view">
                <p>Side View</p>
              </div> */}
            </div>
          </div>

          <div className="open-img-gallery">
            <button type="button" classbtn btn-link>
              <u>
                <b>Open image gallerys</b>
              </u>
            </button>

          </div>

        </div>
      </div>


    </>
  );
}

export default Processtracker;
