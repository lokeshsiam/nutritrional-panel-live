import React, { useEffect, useState, useRef } from "react";
import { Col, Row, Button, Container, Form, InputGroup, Card } from "react-bootstrap";

import { useLocation, useHistory } from "react-router-dom";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";

import PlaceholderImage from "../assets/images/placeholder.png";
import ModalPop from "../components/modals";
import Editor from "../components/Editor";
import { postMethod, getMethod, tokenError } from "../Library";
import ChatList from "./chatList";
import ChatData from "./chatData";
import { ProgressTrackerBody } from "./manageuserprogress";
import GalleryBox from "../components/GalleryBox";


import { Link } from 'react-router-dom';
import "../App.css";
import Down from "../assets/images/dropdown.png";
import Edit from "../assets/images/editimg.png";
import Accordion from 'react-bootstrap/Accordion'
import ViewDetails from "../pages/Viewdetails";
import Modal from 'react-modal'


export default function UserChatPage(props) {
  const location = useLocation();
  const [userInsights, setUserInsights] = useState();
  const [modalShow, setModalShow] = useState(false);
  const [modalShowMeal, setModalShowMeal] = useState(false);
  const [modalShowProg, setModalShowProg] = useState(false);
  const [imageModal, setImageModal] = useState(false);

  const [description, setDescription] = useState("");
  const [imageUrl, setImageUrl] = useState([]);

  const [dateInputs, setDateInputs] = useState();
  const [dateInputsProg, setDateInputsProg] = useState();
  const [filterInputs, setFilterInputs] = useState();
  const [notesData, setNotes] = useState([]);
  const [err, setErr] = useState(false);
  const [mealTrackerData, setMealTrackerData] = useState();
  const [mealLoading, setMealLoading] = useState(false);
  const [progTrackID, setProgTrackID] = useState([]);
  const [imageId, setImageId] = useState([]);
  const [listIds, setListIds] = useState([]);


  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [isOpenOne, setIsOpenOne] = useState(false);


  const [id, setUserId] = useState(
    location.state && location.state.user_id ? location.state.user_id : 0
  );
  const [name, setUsername] = useState(
    location.state && location.state.user_name
      ? location.state.user_name
      : "Unknown"
  );
  const [image, setUserImage] = useState(
    location.state && location.state.user_image_url
      ? location.state.user_image_url
      : null
  );

  const history = useHistory();
  const searchBoxRef = useRef(null);

  const [photos, setPhotos] = useState([]);
  const [captions, setCaptions] = useState([]);

  const getUserInsight = async (type) => {
    const data = {
      url: "user_insight",
      body: {
        user_id: id,
      },
    };
    const newData = await postMethod(data);
    if (newData.status == 1) {
      if (newData.data && newData.data.insights) {
        // console.log("data", newData.data.insights);
        setUserInsights(newData.data);
      } else {
        setUserInsights();
      }
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  const minTwoDigit = (n) => {
    return (n < 10 ? "0" : "") + n;
  };

  const formatDates = (date) => {
    const day = minTwoDigit(date.getDate());
    const month = minTwoDigit(date.getMonth() + 1);
    const year = date.getFullYear();
    let val = `${year}-${month}-${day}`;
    return val;
  };

  const selectUploadDt = (dateSel, name) => {
    const date = new Date(`${dateSel}`);
    const chkDt = date?.getDate();
    if (chkDt > 0) {
      let val = formatDates(date);
      if (name == "meal") {
        fetchMealData(val, id);
        setDateInputs(dateSel);
      } else if (name == "prog") {
        fetchProgData(val, id);
        setDateInputsProg(dateSel);
      }
    } else {
      setDateInputs();
    }
  };

  useEffect(() => {
    if (location?.state?.user_id) {
      // console.log("id,image,name", location);
      // setUserId(location.state.user_id)
      // setUserImage(location.state.user_image_url)
      // setUsername(location.state.user_name)
      getUserInsight();
    }
  }, [id, name, image]);

  useEffect(() => {
    if (location.state && location.state.user_id) {
      setUserId(location.state.user_id);
      setUsername(location.state.user_name);
      setUserImage(location.state.user_image_url);
      setListIds(location.state.ids);
    } else {
      window.location.href = "/";
    }
    console.log("currentlocation ", location.state);
  }, [location.state]);

  useEffect(() => {
    console.log("in page");
    return () => {
      console.log("exit page");
    };
  }, []);

  useEffect(async () => {
    if (filterInputs) {
      const data = {
        url: "view_user_meal_tracker_by_date",
        body: {
          user_id: id,
          date: filterInputs,
        },
      };
      const newData = await postMethod(data);
      if (newData.status == 1) {
        // console.log("mealtrackerdata", newData);
        setMealTrackerData(newData.data[0]);
      } else if (newData.status === false) {
        tokenError(newData.error);
      }
    }
  }, [filterInputs, setFilterInputs, id, name, image]);

  const closeModal = () => {
    setModalShow(false);
    setErr(false);
  };
  const closeModalMeal = () => {
    setModalShowMeal(false);
    setMealTrackerData();
    setMealLoading(false);
    setDateInputs();
  };
  const closeModalProg = () => {
    setModalShowProg(false);
    setDateInputsProg();
    setProgTrackID([]);
  };

  const closeImageModal = () => {
    setImageModal(false);
    // setDateInputsProg();
    setImageId([]);
  };
  const gettypedData = (data) => {
    // console.log("data des", data);
    setDescription(data);
    // let form = { ...formInputs };
    // form.description = data;
    // setFormInputs(form);
  };

  const onSaveNotes = async () => {
    const user = localStorage.getItem("user");
    const newUser = JSON.parse(user);
    if (description) {
      setErr(false);
      const data = {
        url: "add_note_for_user",
        body: {
          user_id: id,
          note: description,
        },
      };
      const newData = await postMethod(data);
      if (newData.status === 1) {
        setDescription("");
        getNotes();
      }
    } else {
      setErr(true);
    }
  };

  const getNotes = async () => {
    const user = localStorage.getItem("user");
    const newUser = JSON.parse(user);
    if (newUser.id) {
      const data = {
        url: "view_note_by_user_id",
        body: {
          user_id: id,
        },
      };
      const newData = await postMethod(data);
      if (newData.status === 1) {
        setNotes(newData.data);
      }
    }
  };

  function createMarkup(data) {
    return { __html: data };
  }

  const dateFormatter = (cell) => {
    let date = new Date(`${cell}`);
    // let year = date.getFullYear();
    // let month = minTwoDigit(date.getMonth() + 1);
    // let day = minTwoDigit(date.getDate());
    function formatAMPM(date) {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let ampm = hours >= 12 ? "PM" : "AM";
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? "0" + minutes : minutes;
      let strTime = hours + ":" + minutes + " " + ampm;
      return strTime;
    }
    return (
      <>
        <span>
          {/* {`${year}-${month}-${day}: `} */}
          {formatAMPM(date)}
        </span>
      </>
    );
  };

  const modalBodyProg = () => {
    return <>{progTrackID && <ProgressTrackerBody user_id={progTrackID} />}</>;
  };
  const modalImage = () => {
    return (
      <div>
        <Col md="12" className="position-relative">
          <div className="d-flex align-items-center justify-content-center mwx-400 ml-auto mr-auto">
            <img src={imageUrl} width="100%" position="center" />
          </div>
          <span className="dt-overlay"></span>
          <GalleryBox
            images={imageUrl}
            // captions={captions}
            position="center"
          />
          {/* <div className="d-flex mt-4 float-right">
          <Button
            variant="primary"
            className="mr-3"
            onClick={closeImageModal}
          >
            Cancel
          </Button>
                </div> */}
        </Col>
      </div>
    );
  };
  const modalBodyMeal = () => {
    return (
      <>
        <Row className="p-3">
          <Col sm="12">
            {/* <h4 className="mt-0">Meal Images</h4> */}
            {mealLoading ? (
              <p>Loading...</p>
            ) : mealTrackerData?.allImages?.length > 0 ? (
              <>
                <Row className="mealTrackerImage mealTrackerImagePop position-relative mt-3">
                  {mealTrackerData?.allImages?.map((item, index) => (
                    <Col md="4" className="position-relative" key={index}>
                      <img
                        src={
                          item.image !== "No Image"
                            ? item.image
                            : PlaceholderImage
                        }
                        width="100%"
                      />
                      <span className="label">{item.name}</span>
                      <span className="dt-overlay"></span>
                    </Col>
                  ))}
                </Row>
                <GalleryBox
                  images={photos}
                  captions={captions}
                  position="center"
                />
                {mealTrackerData?.note && (
                  <>
                    <h4 className="mt-3">User Notes</h4>
                    <div
                      dangerouslySetInnerHTML={createMarkup(
                        mealTrackerData.note
                      )}
                    />
                  </>
                )}
              </>
            ) : (
              <p className="col">
                Oops!! There is no search result for this user based on the
                criteria.
              </p>
            )}
          </Col>

          {/* <Col md="4">
            <div className="dateBox d-flex align-items-center justify-content-around">
              <h6 className="mb-0 textNoWrap">Filter By</h6>
              <InputGroup className="d-flex align-items-center mwx-60">
                <Form.Control
                  type="text"
                  value={filterInputs ? filterInputs : ""}
                  plaintext
                  readOnly
                />
                <InputGroup.Append>
                  <FontAwesomeIcon icon={faCalendarAlt} />
                </InputGroup.Append>
              </InputGroup>
            </div>
          </Col> */}
        </Row>
      </>
    );
  };

  const modalBody = () => {
    return (
      <div className="p-3">
        <div className="notesBox customScrollBar">
          {notesData.length > 0 ? (
            notesData.map((note, ind) => {
              return (
                <div className="mb-3" key={ind}>
                  <div dangerouslySetInnerHTML={createMarkup(note.note)} />
                  <label className="mb-0">
                    {dateFormatter(note.created_at)}
                  </label>
                  <hr />
                </div>
              );
            })
          ) : (
            <p>No data found</p>
          )}
        </div>
        <br />
        <div className="my-3">
          <Editor text={description} data={gettypedData} />
          {err && !description ? (
            <label className="font-sm text-danger">
              This field is required
            </label>
          ) : (
            ""
          )}
        </div>
        <Button
          onClick={() => onSaveNotes()}
          className="mt-2"
          variant="primary"
        >
          Save notes
        </Button>
      </div>
    );
  };

  // const mealImageView = () => {
  //   setModalShowMeal(true);
  // };

  const selectChatData = (user) => {
    // console.log("selecting", user);
    setUserId(user.id);
    setUsername(user.first_name);
    setUserImage(user.image_url);
    // console.log("after selecting", id, name);
    // setUserImage(user.user_image_url)
    updateLocationState(user);
  };

  const updateLocationState = (user) => {
    // console.log("im running");
    history.replace({
      state: {
        user_id: user.id,
        user_image_url: user.image_url,
        user_name: user.first_name,
      },
    });
  };

  const getMealTrackerByTag = async (body) => {
    const data = {
      url: "view_user_meal_tracker_by_date",
      body: { ...body },
    };
    const newData = await postMethod(data);
    setMealLoading(false);
    if (newData.status == 1) {
      setMealTrackerData(newData.data[0]);
      updateGallery(newData.data[0].allImages);
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  const updateGallery = (data) => {
    let phts = [];
    let cpts = [];
    data.forEach((item) => {
      phts.push(item.image);
      cpts.push(`${item.name} - ${item.date}`);
    });
    // console.log("photo ", phts, cpts);
    setPhotos(phts);
    setCaptions(cpts);
  };

  const fetchMealData = (tagDate, tagID) => {
    const data = {
      date: tagDate,
      user_id: parseInt(tagID),
    };
    setModalShowMeal(true);
    setMealLoading(true);
    getMealTrackerByTag(data);
  };

  const fetchProgData = (tagDate, tagID) => {
    setProgTrackID([tagID, tagDate]);
    setModalShowProg(true);
  };
  const fetchImageData = (tagDate, tagID, imageId) => {
    let arr = [];
    arr.push(imageId);
    setImageId([tagID, tagDate]);
    setImageUrl(arr);
    setImageModal(true);
  };
  const chooseTag = (e) => {
    const tagType = e.target.getAttribute("datatype");
    const tagDate = e.target.getAttribute("datadate");
    const tagID = e.target.getAttribute("dataid");
    const imageId = e.target.getAttribute("imageID");
    // setModalShowMeal(true);
    // console.log(tagType, tagDate, tagID);

    if (tagType == "meal-tracker") {
      fetchMealData(tagDate, tagID);
    }

    if (tagType == "progress-tracker") {
      fetchProgData(tagDate, tagID);
    }
    if (tagType == "image") {
      fetchImageData(tagDate, tagID, imageId);
    }
  };
  // console.log()
  return (
    <main className="chat-tracker-page">
      <section className="rightContent mtracker p-0">
        <Container className="container_chat">
          <div className="m-0 fullHeight d-flex custom-chat">
            <div className="LeftBox ">
              <div className="h-100 d-flex">
                <div className=" chat_list_col p-0 border-right">
                  <ChatList
                    selectUser={selectChatData}
                    searchBoxRef={searchBoxRef}
                    id={id}
                    selectedIds={listIds}
                  />
                </div>
                <div className="mt-3 d-flex flex-column chatEncBox">
                  <ChatData
                    data={id ? location : ""}
                    id={id}
                    user_image_url={image}
                    user_name={name}
                    searchBoxRef={searchBoxRef}
                    chooseTag={chooseTag}
                  />
                </div>
              </div>
            </div>
            {/* <div md="3" className="pr-0 border-left rightBox">
              <div className="aboutUserPanel customScrollBar pt-4">
                <h4 className="mb-4">About the User</h4>
                <p>
                  Email:{" "}
                  {userInsights?.user?.email ? (
                    <a href={`mailto:${userInsights.user.email}`}>
                      <u>{userInsights.user.email}</u>
                    </a>
                  ) : (
                    "N/A"
                  )}
                </p>
                {userInsights ? (
                  <>
                    <h6>
                      <u>User Insights</u>
                    </h6>
                    <div className="mt-3 mb-4 userInsightBox border border-dark rounded px-3 py-3">
                      <Row>
                        <Col md="5">
                          <b>Name</b>
                        </Col>
                        <Col>
                          {userInsights?.user?.name
                            ? userInsights?.user?.name
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Gender</b>
                        </Col>
                        <Col>
                          {userInsights?.insights?.gender
                            ? userInsights?.insights?.gender
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Age</b>
                        </Col>
                        <Col>
                          {userInsights && userInsights?.insights?.age
                            ? userInsights?.insights?.age
                            : "N/A"}
                        </Col>
                      </Row>

                      <Row>
                        <Col md="5">
                          <b>Height</b>
                        </Col>
                        <Col>
                          {userInsights && userInsights?.insights?.height
                            ? `${userInsights?.insights?.height} ${userInsights?.insights?.height_sign}`
                            : "N/A"}{" "}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Weight</b>
                        </Col>
                        <Col>
                          {userInsights && userInsights?.insights?.weight
                            ? `${userInsights?.insights?.weight} ${userInsights?.insights?.weight_sign}`
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Diet Type</b>
                        </Col>
                        <Col>
                          {userInsights && userInsights?.insights?.diet_type
                            ? userInsights?.insights?.diet_type
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Goal</b>
                        </Col>
                        <Col>
                          {userInsights && userInsights?.insights?.goal
                            ? userInsights?.insights?.goal
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Level</b>
                        </Col>
                        <Col>
                          {userInsights?.insights?.activity_level
                            ? userInsights?.insights?.activity_level
                            : "N/A"}
                        </Col>
                      </Row>

                      <Row>
                        <Col md="5">
                          <b>Program</b>
                        </Col>
                        <Col>
                          {userInsights?.program?.program_name
                            ? userInsights?.program?.program_name
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Week</b>
                        </Col>
                        <Col>
                          {userInsights?.userCurrentWeek
                            ? userInsights.userCurrentWeek
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col md="5">
                          <b>Country</b>
                        </Col>
                        <Col>
                          {userInsights?.user?.timezone
                            ? userInsights.user?.timezone
                            : "N/A"}
                        </Col>
                      </Row>
                      <Row>
                        <Col className="text-center mt-3 mb-2">
                          <b>Health Conditions</b>
                        </Col>
                      </Row>
                      <div className="d-flex flex-wrap">
                        {userInsights &&
                        userInsights?.insights?.conditions &&
                        userInsights?.insights?.conditions.length > 0 ? (
                          userInsights?.insights?.conditions.map(
                            (item, index) => (
                              <div className="items width-auto" key={index}>
                                {item}
                              </div>
                            )
                          )
                        ) : (
                          <Col>N/A</Col>
                        )}
                      </div>
                    </div>
                  </>
                ) : (
                  <h6>
                    <u>No User Insights</u>
                  </h6>
                )}

                <div>
                  <h6 className="mt-5 mb-3">
                    <u>Progress Tracker</u>
                  </h6>
                  <DatePicker
                    onChange={(date) => selectUploadDt(date, "prog")}
                    selected={dateInputsProg ? dateInputsProg : null}
                    maxDate={new Date()}
                    name="date"
                    dateFormat="yyyy/MM/dd"
                    placeholderText="Choose a date"
                    autoComplete="off"
                  />
                </div>

                {/* <div className="text-center">
                  <Button
                    className="w-100"
                    onClick={() => {
                      setModalShow(true);
                      getNotes();
                    }}
                    variant="primary"
                  >
                    Add a note
                  </Button>
                </div> */}

            {/* <h6 className="mt-5 mb-3">
                  <u>Meal Tracker</u>
                </h6>
                <DatePicker
                  onChange={(date) => selectUploadDt(date, "meal")}
                  selected={dateInputs ? dateInputs : null}
                  maxDate={new Date()}
                  name="date"
                  dateFormat="yyyy/MM/dd"
                  placeholderText="Choose a date"
                  autoComplete="off"
                /> */}
            {/* {dateInputs ? (
                  <>
                    <Row className="mt-2 flex-wrap mealTrackerImage position-relative">
                      <Button
                        variant="link"
                        className="overlayButton"
                        onClick={mealImageView}
                      ></Button>
                      {mealTrackerData &&
                      mealTrackerData.allImages &&
                      mealTrackerData.allImages.length > 0 ? (
                        mealTrackerData.allImages.map((item, index) => (
                          <Col md="6" key={index}>
                            <img
                              src={
                                item.image !== "No Image"
                                  ? item.image
                                  : PlaceholderImage
                              }
                              width="100%"
                            />
                            <span className="dt">{item.date}</span>
                            <span className="dt-overlay"></span>
                          </Col>
                        ))
                      ) : (
                        <Col>
                          <p>No Data Found</p>
                        </Col>
                      )}
                    </Row>
                  </>
                ) : (
                  <p className="mt-2"></p>
                )} */}
            {/* <br />
                <br />
                <br />
              </div>
            </div>*/}

            {/* my code for UserChat */}

            <div className="right">

              <div className="user-box-1">
                <div className="mt-1 mb-0 px-3 py-3">
                  <b className="name">venkatnarasimharajuvaripeta</b>
                  <p className="begin">Beginner</p>

                  <Row>
                    <Col>
                      <b>Gender</b>
                    </Col>
                    <Col>
                      Male
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <b>Age</b>
                    </Col>
                    <Col>
                      23
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <b>Program</b>
                    </Col>
                    <Col>
                      Reboot leve-1
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <b>Height</b>
                    </Col>
                    <Col>
                      150 cm
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <b>Weight</b>
                    </Col>
                    <Col>
                      70 kg
                    </Col>
                  </Row>

                  {/* page Navigation */}
                  <div className="page-navigation">
                    <p onClick={() => setIsOpenOne(true)} >
                      View Details
                    </p>
                  </div>

                </div>
              </div>
              {isOpenOne && <ViewDetails close={setIsOpenOne} />}
              <br></br>

              <div className="user-box-2">
                <Accordion defaultActiveKey="1" className="user-box-2" >
                  <Card className="card-head">
                    <Card.Header className="Card-header ">
                      <Accordion.Toggle as={Button} variant="link" eventKey="0" >
                        <Row>
                          <Col className="health">
                            <div className="health-text">
                              <b>Health Conditions</b>
                            </div>
                            <div className="img_chat_box_dropicon" >
                              <img src={Down} />
                            </div>
     
                          </Col>
                        </Row>
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body >
                        <Row>
                          <Col className="none">
                            None
                          </Col>
                        </Row>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>

              </div>

              <br></br>

              <div className="notes">
                Notes
              </div>

              <div className="user-box-3">
                <div className="mt-7  mb-0 userInsightBox  px-3 py-3 ">

                  <img className="img_chat_box_edit" src={Edit} />

                  <textarea className="lorem">

                    Lorem ipsum dolor sisec scing Aenean massa. Cum sociis natoque penatibus et
                    magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
                    pellentesque eu

                  </textarea>
                </div>
              </div>
              <button type="submit" className="mb-2 save">save</button>
            </div>


          </div>
        </Container>
        <ModalPop
          show={modalShow}
          onHide={() => {
            closeModal();
          }}
          modalcontent={modalBody()}
          modalhead={`Notes`}
        />
        <ModalPop
          show={modalShowMeal}
          onHide={() => {
            closeModalMeal();
          }}
          modalcontent={modalBodyMeal()}
          modalhead={`Meal Image View`}
        />
        <ModalPop
          show={modalShowProg}
          onHide={() => {
            closeModalProg();
          }}
          modalcontent={modalBodyProg()}
          modalhead={`Progress Tracker`}
        />
        <ModalPop
          show={imageModal}
          onHide={() => {
            closeImageModal();
          }}
          modalcontent={modalImage()}
          modalhead={`Image Tracker`}
        />
      </section>
    </main>
  );
}
