import React, { Component } from "react";
import { Col, Row, Button, Container, Form } from "react-bootstrap";
import Pusher from "pusher-js";

// import { useLocation } from "react-router-dom";

// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { postMethod, copyObjs } from "../Library";
import ModalPop from "../components/modals";

import PlaceholderImage from "../assets/images/placeholder.png";
import { set, update } from "lodash";
import { pusherKey } from "../Library/configs";

export default class chatData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      msg: "",
      type: "",
      fromDateChange: "",
      toDate: "",
      msgData: [],
      show: false,
      dateMsg: "",
      id: "",
      pusher_channel: null,
      pusher_name: null,
    };
  }
  componentDidMount() {
    // console.log("props user", this.props);
    const { msgData } = this.state;
    if (this.props.id) {
      this.setState(
        {
          id: this.props.id,
        },
        () => {
          this.getMsgData();
          const id = localStorage.getItem("user");
          const UserId = id ? JSON.parse(id).id : "";
          const pusher = new Pusher(pusherKey, {
            cluster: "ap2",
            encrypted: true,
          });
          // console.log(pusher);
          const channel = pusher.subscribe(`ch-${UserId}`);
          // console.log("channel", msgData);
          this.setState({
            pusher_channel: channel,
            pusher_name: pusher,
          });
          channel.bind("meal-comment-data", (data) => {
            const newArr = copyObjs(this.state.msgData);
            // console.log("msgDatas", this.state.msgData, newArr);
            if (data.comment_history) {
              const updateId = parseInt(data.value.sender_id);
              // console.log(
              //   "inside cht",
              //   updateId,
              //   parseInt(this.state.id),
              //   this.state.msgData,
              //   newArr
              // );
              if (updateId === parseInt(this.state.id)) {
                // const newMsgs = msgData
                newArr.unshift(data.value);
                // console.log("inside cht updating", msgData, newArr);
                this.setState({
                  msgData: newArr,
                });
              }
            }
            // console.log("notificatrion bind", data, msgData);
          });
        }
      );
    }
  }

  componentWillUnmount() {
    if (this.state.pusher_name && this.state.pusher_channel) {
      this.state.pusher_name.unsubscribe(this.state.pusher_channel);
      this.state.pusher_channel.unbind();
      // console.log("pusher unsubsribed user feedback");
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const currentSlug = nextProps.id;
    const oldSlug = prevState.id;
    if (currentSlug !== oldSlug) {
      return { id: currentSlug };
    } else {
      return null;
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.id !== this.state.id) {
      this.getMsgData();
    }
  }

  textChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      msg: value,
    });
  };

  enterMsg = async (e) => {
    const { msg } = this.state;
    e.preventDefault();
    if (msg.length > 0) {
      const data = {
        url: "nutritionist_meal_tracker_comment_add",
        body: {
          message: msg,
          sender_id: this.state.id,
        },
      };
      const newData = await postMethod(data);
      if (newData.status === 1) {
        this.setState({
          msg: "",
        });
        this.getMsgData();
      }
    }
  };

  getMsgData = async () => {
    const { fromDate, toDate } = this.state;
    // console.log("getting msg Data");
    const data = {
      url: "nutritionist_meal_tracker_comment_list",
      body: {
        user_id: this.state.id,
        filter_type: "overall",
        from_date: fromDate,
        to_date: toDate,
      },
    };
    const newData = await postMethod(data);
    if (newData.status === 1) {
      // console.log("got msg Data");
      this.setState({
        msgData: newData.data.reverse(),
      });
    } else {
      this.setState({
        msgData: [],
      });
    }
  };

  getFilterMsg = async (val) => {
    const { fromDate, toDate } = this.state;
    const data = {
      url: "nutritionist_meal_tracker_comment_list",
      body: {
        user_id: this.state.id,
        filter_type: val,
        from_date: fromDate,
        to_date: toDate,
      },
    };
    const newData = await postMethod(data);
    if (newData.status === 1) {
      this.setState({
        msgData: newData.data.reverse(),
      });
      if (val == "custom_date") {
        this.setState({
          show: false,
        });
      }
    } else {
      this.setState({
        dateMsg: newData.message,
      });
    }
  };

  filterChange = (e) => {
    const { name, value } = e.target;
    // console.log("date", value);
    this.setState({
      type: value,
    });
    if (value === "custom_date") {
      this.setState({
        show: true,
      });
    } else {
      this.getFilterMsg(value);
    }
  };

  fromDateChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      fromDate: value,
    });
  };

  toDateChange = (e) => {
    this.setState({
      toDate: e.target.value,
    });
  };

  closeModal = () => {
    this.setState({
      show: false,
    });
  };

  minTwoDigit = (n) => {
    return (n < 10 ? "0" : "") + n;
  };

  dateFormatter = (cell) => {
    let date = new Date(`${cell}`);
    let year = date.getFullYear();
    let month = this.minTwoDigit(date.getMonth() + 1);
    let day = this.minTwoDigit(date.getDate());
    function formatAMPM(date) {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let ampm = hours >= 12 ? "PM" : "AM";
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? "0" + minutes : minutes;
      let strTime = hours + ":" + minutes + " " + ampm;
      return strTime;
    }
    return (
      <>
        <span>
          {`${year}-${month}-${day} `}
          {formatAMPM(date)}
        </span>
      </>
    );
  };

  getFirstLetter = (name) => {
    let x = name.split("");
    return x[0];
  };

  modalBody = () => {
    return (
      <div className="p-3">
        <div className="d-flex align-items-center">
          <Form.Group className="w-100 mb-0 mx-3 mwx-300" controlId="email">
            <Form.Label>From</Form.Label>
            <Form.Control
              type="date"
              name="from_date"
              size="lg"
              className="mb-0"
              onChange={this.fromDateChange}
              placeholder="Search"
            />
          </Form.Group>
          <Form.Group className="w-100 mb-0 mx-3 mwx-300" controlId="email">
            <Form.Label>To</Form.Label>
            <Form.Control
              type="date"
              name="to_date"
              size="lg"
              onChange={this.toDateChange}
              className="mb-0 ml-0"
              placeholder="Search"
            />
          </Form.Group>
        </div>
        {this.state.dateMsg && (
          <p className="text-left pl-3 mt-2 mb-0 text-capitalize err-feedback">
            {this.state.dateMsg}
          </p>
        )}
        <br />
        <Row className="ml-3">
          <Button
            onClick={this.closeModal}
            className="mt-2"
            variant="secondary"
          >
            Cancel
          </Button>
          <Button
            onClick={() => this.getFilterMsg("custom_date")}
            className="mt-2 ml-3"
            variant="primary"
          >
            Apply
          </Button>
        </Row>
      </div>
    );
  };

  render() {
    return (
      <div className="m-3">
        {/* <div className="m-3 h-100 d-flex flex-column justify-content-between"> */}
        <div className="user-profile d-flex align-items-center justify-content-between">
          <div className="d-flex align-items-center">
            <img
              src={
                this.props.user_image_url
                  ? this.props.user_image_url
                  : PlaceholderImage
              }
              className="profile-pic"
            />
            <div className="ml-3">
              <h4 className="mb-0">{this.props.user_name}</h4>
            </div>
          </div>
          {this.props.filter ? (
            <div className="d-flex align-items-center">
              <h6 className="mr-3 mb-0">Filter by</h6>
              <Form.Group controlId="program" className="mb-0 mr-3 mw-200">
                <Form.Control
                  as="select"
                  name="program"
                  onChange={this.filterChange}
                >
                  <option value="overall">Overall</option>
                  <option value="day">Lastday</option>
                  <option value="week">Lastweek</option>
                  <option value="month">Lastmonth</option>
                  <option value="custom_date">Custom date</option>
                </Form.Control>
              </Form.Group>
            </div>
          ) : (
            ""
          )}
        </div>
        <div className="messaging">
          <div className="inbox_msg">
            <div className="mesgs">
              <div className="msg_history customScrollBar" id="content">
                {/* Senders Message */}
                {this.state.msgData.length > 0
                  ? this.state.msgData.map((x, i) => {
                      if (x.sender_by === "nutritionist") {
                        return (
                          <div
                            className="d-flex justify-content-start mb-3"
                            key={i}
                          >
                            <div className="msg_box msg_box_send">
                              {x.receiver.image_url ? (
                                <img
                                  src={x.receiver.image_url}
                                  className="chat-profile-pic mr-2"
                                />
                              ) : (
                                <div className="chatNameBox">
                                  {this.getFirstLetter(x.receiver.first_name)}
                                </div>
                              )}
                              <div>
                                <span className="font-md font-weight-bold mb-0 text-muted">
                                  {x.message}
                                </span>
                                <p className="font-12 text-muted mt-1 mb-0">
                                  {this.dateFormatter(x.created_at)}
                                </p>
                              </div>
                            </div>
                          </div>
                        );
                      } else {
                        {
                          /* Reciever Message */
                        }
                        return (
                          <div className="d-flex justify-content-end mb-3">
                            <div className="msg_box msg_box_rec">
                              <div>
                                <span className="font-md font-weight-bold text-small mb-0 text-dark">
                                  {x.message}
                                </span>
                                <p className="font-12 text-muted mt-1 mb-0">
                                  {this.dateFormatter(x.created_at)}
                                </p>
                              </div>
                              {x.sender.image_url ? (
                                <img
                                  src={x.sender.image_url}
                                  className="chat-profile-pic ml-2"
                                />
                              ) : (
                                <div className="chatNameBox">
                                  {this.getFirstLetter(x.sender.first_name)}
                                </div>
                              )}
                            </div>
                          </div>
                        );
                      }
                    })
                  : "No messages yet"}
              </div>
              {/* <!-- Typing area --> */}
              <form onSubmit={(e) => this.enterMsg(e)}>
                <div className="type_msg">
                  <div className="input_fileds">
                    <div className="bg-light">
                      <div className="input-group">
                        <input
                          type="text"
                          value={this.state.msg}
                          onChange={(e) => this.textChange(e)}
                          autoComplete="off"
                          placeholder="Type a message"
                          name="message"
                          className="form-control rounded-0 border-0 py-4 bg-white"
                        />
                        <button
                          type="submit"
                          id="button-addon2"
                          tabIndex="-1"
                          className="btn btn-link"
                        >
                          {" "}
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xlink="http://www.w3.org/1999/xlink"
                            aria-hidden="true"
                            focusable="false"
                            width="1em"
                            height="1em"
                            preserveAspectRatio="xMidYMid meet"
                            viewBox="0 0 1792 1792"
                          >
                            <path
                              d="M1764 11q33 24 27 64l-256 1536q-5 29-32 45q-14 8-31 8q-11 0-24-5l-453-185l-242 295q-18 23-49 23q-13 0-22-4q-19-7-30.5-23.5T640 1728v-349l864-1059l-1069 925l-395-162q-37-14-40-55q-2-40 32-59L1696 9q15-9 32-9q20 0 36 11z"
                              fill="#626262"
                            />
                          </svg>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <ModalPop
          show={this.state.show}
          onHide={this.closeModal}
          modalcontent={
            <div className="p-3">
              <div className="d-flex align-items-center">
                <Form.Group
                  className="w-100 mb-0 mx-3 mwx-300"
                  controlId="email"
                >
                  <Form.Label>From</Form.Label>
                  <Form.Control
                    type="date"
                    name="from_date"
                    size="lg"
                    className="mb-0"
                    onChange={this.fromDateChange}
                    placeholder="Search"
                  />
                </Form.Group>
                <Form.Group
                  className="w-100 mb-0 mx-3 mwx-300"
                  controlId="email"
                >
                  <Form.Label>To</Form.Label>
                  <Form.Control
                    type="date"
                    name="to_date"
                    size="lg"
                    onChange={this.toDateChange}
                    className="mb-0 ml-0"
                    placeholder="Search"
                  />
                </Form.Group>
              </div>
              {this.state.dateMsg && (
                <p className="text-left pl-3 mt-2 mb-0 text-capitalize err-feedback">
                  {this.state.dateMsg}
                </p>
              )}
              <br />
              <Row className="ml-3">
                <Button
                  onClick={this.closeModal}
                  className="mt-2"
                  variant="secondary"
                >
                  Cancel
                </Button>
                <Button
                  onClick={() => this.getFilterMsg("custom_date")}
                  className="mt-2 ml-3"
                  variant="primary"
                >
                  Apply
                </Button>
              </Row>
            </div>
          }
          modalhead={`Custom date`}
        />
      </div>
    );
  }
}
