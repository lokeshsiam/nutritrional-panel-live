import React, { useEffect, useState } from "react";
import { Col, Row, Button, Container, Form, InputGroup } from "react-bootstrap";

import { useLocation } from "react-router-dom";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";

import PlaceholderImage from "../assets/images/placeholder.png";
import ModalPop from "../components/modals";
import Editor from "../components/Editor";
import { postMethod, getMethod, tokenError } from "../Library";
import ChatList from "./chatList";
import MessageList from "./messageList";

export default function MealTracker() {
  const [userInsights, setUserInsights] = useState();
  const [modalShow, setModalShow] = useState(false);
  const [modalShowMeal, setModalShowMeal] = useState(false);
  const [description, setDescription] = useState("");
  const [dateInputs, setDateInputs] = useState();
  const [filterInputs, setFilterInputs] = useState();
  const [notesData, setNotes] = useState([]);
  const [err, setErr] = useState(false);
  const [mealTrackerData, setMealTrackerData] = useState();

  const location = useLocation();

  const getUserInsight = async (type) => {
    const data = {
      url: "user_insight",
      body: {
        user_id: location.state.user_id,
      },
    };
    const newData = await postMethod(data);
    if (newData.status == 1) {
      if (newData.data && newData.data.insights) {
        // console.log("data", newData.data.insights);
        setUserInsights(newData.data.insights);
      }
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  const minTwoDigit = (n) => {
    return (n < 10 ? "0" : "") + n;
  };

  const formatDates = (date) => {
    const day = minTwoDigit(date.getDate());
    const month = minTwoDigit(date.getMonth() + 1);
    const year = date.getFullYear();
    let val = `${year}-${month}-${day}`;
    return val;
  };

  const selectUploadDt = (dateSel, name) => {
    const date = new Date(`${dateSel}`);
    const chkDt = date.getDate();
    if (chkDt > 0) {
      let val = formatDates(date);
      setFilterInputs(val);
      setDateInputs(dateSel);
    } else {
      setDateInputs();
    }
  };

  useEffect(async () => {
    if (filterInputs) {
      const data = {
        url: "view_user_meal_tracker_by_date",
        body: {
          user_id: location.state.user_id,
          date: filterInputs,
        },
      };
      const newData = await postMethod(data);
      if (newData.status == 1) {
        // console.log("mealtrackerdata", newData);
        setMealTrackerData(newData.data[0]);
      } else if (newData.status === false) {
        tokenError(newData.error);
      }
    }
  }, [filterInputs, setFilterInputs]);

  useEffect(() => {
    if (location.state.user_id) {
      getUserInsight();
    }
  }, []);

  const closeModal = () => {
    setModalShow(false);
    setErr(false);
  };
  const closeModalMeal = () => {
    setModalShowMeal(false);
  };

  const gettypedData = (data) => {
    // console.log("data des", data);
    setDescription(data);
    // let form = { ...formInputs };
    // form.description = data;
    // setFormInputs(form);
  };

  const onSaveNotes = async () => {
    const user = localStorage.getItem("user");
    const newUser = JSON.parse(user);
    if (description) {
      setErr(false);
      const data = {
        url: "add_note_for_user",
        body: {
          user_id: location.state.user_id,
          note: description,
        },
      };
      const newData = await postMethod(data);
      if (newData.status === 1) {
        setDescription("");
        getNotes();
      }
    } else {
      setErr(true);
    }
  };

  const getNotes = async () => {
    const user = localStorage.getItem("user");
    const newUser = JSON.parse(user);
    if (newUser.id) {
      const data = {
        url: "view_note_by_user_id",
        body: {
          user_id: location.state.user_id,
        },
      };
      const newData = await postMethod(data);
      if (newData.status === 1) {
        setNotes(newData.data);
      }
    }
  };

  function createMarkup(data) {
    return { __html: data };
  }

  const dateFormatter = (cell) => {
    let date = new Date(`${cell}`);
    // let year = date.getFullYear();
    // let month = minTwoDigit(date.getMonth() + 1);
    // let day = minTwoDigit(date.getDate());
    function formatAMPM(date) {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let ampm = hours >= 12 ? "PM" : "AM";
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? "0" + minutes : minutes;
      let strTime = hours + ":" + minutes + " " + ampm;
      return strTime;
    }
    return (
      <>
        <span>
          {/* {`${year}-${month}-${day}: `} */}
          {formatAMPM(date)}
        </span>
      </>
    );
  };

  const modalBodyMeal = () => {
    return (
      <>
        <Row className="p-3">
          <Col md="8">
            <Row className="mealTrackerImage mealTrackerImagePop position-relative ">
              {mealTrackerData &&
              mealTrackerData.allImages &&
              mealTrackerData.allImages.length > 0 ? (
                mealTrackerData.allImages.map((item, index) => (
                  <Col md="6" key={index}>
                    <img
                      src={
                        item.image !== "No Image"
                          ? item.image
                          : PlaceholderImage
                      }
                      width="100%"
                    />
                    <span className="dt">{item.date}</span>
                    <span className="dt-overlay"></span>
                  </Col>
                ))
              ) : (
                <p>No Images</p>
              )}
            </Row>
            <h4 className="mt-3">User Notes</h4>
            {mealTrackerData && mealTrackerData.note ? (
              <div
                dangerouslySetInnerHTML={createMarkup(mealTrackerData.note)}
              />
            ) : (
              <p>No Notes</p>
            )}
          </Col>
          <Col md="4">
            <div className="dateBox d-flex align-items-center justify-content-around">
              <h6 className="mb-0 textNoWrap">Filter By</h6>
              <InputGroup className="d-flex align-items-center mwx-60">
                <Form.Control
                  type="text"
                  value={filterInputs ? filterInputs : ""}
                  plaintext
                  readOnly
                />
                <InputGroup.Append>
                  <FontAwesomeIcon icon={faCalendarAlt} />
                </InputGroup.Append>
              </InputGroup>
            </div>
          </Col>
        </Row>
      </>
    );
  };

  const modalBody = () => {
    return (
      <div className="p-3">
        <div className="notesBox customScrollBar">
          {notesData.length > 0 ? (
            notesData.map((note, ind) => {
              return (
                <div className="mb-3" key={ind}>
                  <div dangerouslySetInnerHTML={createMarkup(note.note)} />
                  <label className="mb-0">
                    {dateFormatter(note.created_at)}
                  </label>
                  <hr />
                </div>
              );
            })
          ) : (
            <p>No data found</p>
          )}
        </div>
        <br />
        <div className="my-3">
          <Editor text={description} data={gettypedData} />
          {err && !description ? (
            <label className="font-sm text-danger">
              This field is required
            </label>
          ) : (
            ""
          )}
        </div>
        <Button
          onClick={() => onSaveNotes()}
          className="mt-2"
          variant="primary"
        >
          Save notes
        </Button>
      </div>
    );
  };

  const mealImageView = () => {
    setModalShowMeal(true);
  };

  return (
    <main className="chat-tracker-page">
      <section className="rightContent mtracker p-0">
        <Container>
          <Row className="m-0 fullHeight">
            <Col md="9" className="LeftBox">
              <Row className="h-100">
                {/* <Col md='6' className='p-0 border-right'>
                  <ChatList />
                </Col> */}
                <Col className="p-0">
                  <MessageList
                    filter={true}
                    data={location.state.user_id ? location : ""}
                    id={location.state.user_id ? location.state.user_id : ""}
                    user_image_url={
                      location.state.user_image_url
                        ? location.state.user_image_url
                        : ""
                    }
                    user_name={
                      location.state.user_name
                        ? location.state.user_name
                        : "User"
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col md="3" className="pr-0 rightBox">
              <div className="aboutUserPanel customScrollBar pt-4">
                <h4 className="mb-4">About the User</h4>
                <h6>
                  <u>User Insights</u>
                </h6>
                <div className="mt-3 mb-4 border border-dark rounded px-3 py-3">
                  <Row>
                    <Col md="5">
                      <b>Your Goal</b>
                    </Col>
                    <Col>
                      {userInsights && userInsights.goal
                        ? userInsights.goal
                        : "N/A"}
                    </Col>
                  </Row>
                  <Row>
                    <Col md="5">
                      <b>Weight</b>
                    </Col>
                    <Col>
                      {userInsights && userInsights.weight
                        ? userInsights.weight
                        : "N/A"}
                    </Col>
                  </Row>
                  <Row>
                    <Col md="5">
                      <b>Age</b>
                    </Col>
                    <Col>
                      {userInsights && userInsights.age
                        ? userInsights.age
                        : "N/A"}
                    </Col>
                  </Row>
                  <Row>
                    <Col md="5">
                      <b>Diet Type</b>
                    </Col>
                    <Col>
                      {userInsights && userInsights.diet_type
                        ? userInsights.diet_type
                        : "N/A"}
                    </Col>
                  </Row>
                  <Row>
                    <Col className="text-center mt-3 mb-2">
                      <b>Health Conditions</b>
                    </Col>
                  </Row>
                  <div className="d-flex flex-wrap">
                    {userInsights &&
                    userInsights.conditions &&
                    userInsights.conditions.length > 0 ? (
                      userInsights.conditions.map((item, index) => (
                        <div className="items width-auto" key={index}>
                          {item}
                        </div>
                      ))
                    ) : (
                      <Col>N/A</Col>
                    )}
                  </div>
                </div>
                <div className="text-center">
                  <Button
                    className="w-100"
                    onClick={() => {
                      setModalShow(true);
                      getNotes();
                    }}
                    variant="primary"
                  >
                    Add a note
                  </Button>
                </div>

                <h6 className="mt-5 mb-3">
                  <u>Meal Tracker</u>
                </h6>
                <DatePicker
                  onChange={(date) => selectUploadDt(date, "date")}
                  selected={dateInputs ? dateInputs : null}
                  maxDate={new Date()}
                  name="date"
                  dateFormat="yyyy/MM/dd"
                  placeholderText="Choose a date"
                  autoComplete="off"
                />
                {dateInputs ? (
                  <>
                    <Row className="mt-2 flex-wrap mealTrackerImage position-relative">
                      <Button
                        variant="link"
                        className="overlayButton"
                        onClick={mealImageView}
                      ></Button>
                      {mealTrackerData &&
                      mealTrackerData.allImages &&
                      mealTrackerData.allImages.length > 0 ? (
                        mealTrackerData.allImages.map((item, index) => (
                          <Col md="6" key={index}>
                            <img
                              src={
                                item.image !== "No Image"
                                  ? item.image
                                  : PlaceholderImage
                              }
                              width="100%"
                            />
                            <span className="dt">{item.date}</span>
                            <span className="dt-overlay"></span>
                          </Col>
                        ))
                      ) : (
                        <Col>
                          <p>No Data Found</p>
                        </Col>
                      )}
                    </Row>
                  </>
                ) : (
                  <p className="mt-2"></p>
                )}
              </div>
            </Col>
          </Row>
        </Container>
        <ModalPop
          show={modalShow}
          onHide={() => {
            closeModal();
          }}
          modalcontent={modalBody()}
          modalhead={`Notes`}
        />
        <ModalPop
          show={modalShowMeal}
          onHide={() => {
            closeModalMeal();
          }}
          modalcontent={modalBodyMeal()}
          modalhead={`Meal Image View`}
        />
      </section>
    </main>
  );
}
