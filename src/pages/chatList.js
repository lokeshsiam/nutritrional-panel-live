import React, { Component } from "react";
import { Col, Row, Button, Container, Form } from "react-bootstrap";
import { debounce } from "lodash";
import Pusher from "pusher-js";
import moment from "moment";
// import { useLocation } from "react-router-dom";
// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { postMethodNoLoad, postMethod,tokenError } from "../Library/";
import PlaceholderImage from "../assets/images/blackUser.png";
import { pusherKey } from "../Library/configs";
import LabelImg from "../assets/images/labelRed.png";
import PinImg from "../assets/images/pin.png";
import markUnRead from "../assets/images/markUnRead.png";
import readPin from "../assets/images/redPin.png";
import redLabel from "../assets/images/pinRed.png";
import Num from "../assets/images/num.png";
import Down from "../assets/images/caretDown.png";




export default class chatList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      search_name: "",
      count_loaded: false,
      pusher_channel: null,
      pusher_name: null,
      userListLoading: true,
      checkBox:[],
      selectedIds:[],
      filter:'',
      filterId:[]
    };
    this.searchUsers = debounce(this.searchUsers, 1500);
  }

  componentDidMount() {
    if (this.props.id) {
      // this.getUsername();
      this.initPusher();
    }
  if(this.props.selectedIds){
    this.setState({filterId:this.props.selectedIds})
  }

  }
  // componentDidUpdate=()=>{
  //   // this.searchUsers('list_nutritions_user_name');
  // }
  componentWillUnmount() {
    if (this.state.pusher_name && this.state.pusher_channel) {
      this.state.pusher_name.unsubscribe(this.state.pusher_channel);
      this.state.pusher_channel.unbind();
      // console.log("pusher unsubsribed user list");
    }
  }

  initPusher = () => {
    const id = localStorage.getItem("user");
    const UserId = id ? JSON.parse(id).id : "";
    const pusher = new Pusher(pusherKey, {
      cluster: "ap2",
      encrypted: true,
    });
    const channel = pusher.subscribe(`ch-${UserId}`);
    this.setState({
      pusher_channel: channel,
      pusher_name: pusher,
    });
    this.getUsername();
    channel.bind("refersh-api", (data) => {
      
        if (data.refresh_list_nutritions_user_name == 1) {
        this.getUsername();
      }
    });
  };

  onInputChange = (e) => {
    const { name, value } = e.target;
    this.props.searchBoxRef.current.value = value;
    setTimeout(() => {
      this.searchUsers(value,null);
    }, 3000);
  };
  onFilterSortBy =(e)=>{
   let sort = e.target.value
  }

  searchUsers = async (val,b) => {
    var ids = localStorage.getItem('filterIds')
    var array = ids.split(',').map(function(n) {
      return Number(n);
  })
    const data = {
      url: "list_nutritions_user_name",
      body: {
        search: val,
        sort_by:b,
        filtered_users:array

      },
    };
    const newData = await postMethodNoLoad(data);
    if (newData.status === 1) {
      // console.log("chatList", newData);
      this.setState({
        users: newData.data.user_name_list,
      });
      // setUsers(newData.data.user_name_list);
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  getUsername = async () => {
    var ids = localStorage.getItem('filterIds')
    var array = ids.split(',').map(function(n) {
      return Number(n);
  })
    console.log(array,'idsssssssssssssss')
    const data = {
      url: "list_nutritions_user_name",
      body: {
        filtered_users:array
      },
    };
    const newData = await postMethodNoLoad(data);
    this.setState({
      count_loaded: true,
      userListLoading: false,
    });
    if (newData.status === 1) {
      // console.log("chatList", newData);
      this.setState({
        users: newData.data.user_name_list,
      });

      // setUsers(newData.data.user_name_list);
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  // refreshUserList = () => {
  //   this.getUsername();
  // };

  // deleteUserActive = (e) => {
  //   if (
  //     e.target.querySelector(".chatActive").classList.contains("activeUser")
  //   ) {
  //     e.target
  //       .querySelector(".chatActive")
  //       .classList.replace("activeUser", "inactiveUser");
  //   }
  // };

  setTimeAgo = (user) => {
    let string = "";
    // if (user.hasOwnProperty("chat_historys_last")) {
    //   if (user.chat_historys_last) {
    //     let date = user.chat_historys_last.created_at;
    //     // let x = moment(date).fromNow();
    //     let x = moment(date).format("LLL");
    //     // console.log(x);
    //     string = x;
    //   } else {
    //     string = "";
    //   }
    //   // string = user.chat_historys_last.created_at;
    // }
    if (user.hasOwnProperty("last_chat_date")) {
      if (user.last_chat_date) {
        string = moment(user.last_chat_date).format('LLL');
      } else {
        string = "";
      }
    }
    return string;
  };

  diffId = (user) => {
    let x = function () {
      return user.chat_historys_count > 0 ? (
        <div className="chatCount ">
          {user.chat_historys_count > 0 ? user.chat_historys_count : null}
        </div>
      ) : null;
    };
    return x();
  };

  linkClicked = (e, user) => {
    // if()user.id != this.props.id
    e.preventDefault();
    if (user.id != this.props.id) {
      this.props.selectUser(user);
      this.setState({
        count_loaded: false,
      });
    }
  };
  handleFilterInput=(e)=>{
let a = e.target.value
this.setState({setFilter:a})
// console.log(a,'a')
this.handleFilter(a);
  }
  handleFilter=async(a)=>{
    var ids = localStorage.getItem('filterIds')
    var array = ids.split(',').map(function(n) {
      return Number(n);
  })
    const data = {
      url: "list_nutritions_user_name",
      body: {
        sort_by:a,
        filtered_users:array

      },
    };
    const newData = await postMethodNoLoad(data);
    if (newData.status === 1) {
      // console.log("chatList", newData);
      this.setState({
        users: newData.data.user_name_list,
      });
      // setUsers(newData.data.user_name_list);
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  }
  handleCheck=(e,i, a)=>{   
    const {name, checked} = e.target;
    if(name=="allSelect"){
      let tempUser = this.state.users.map((user)=>{return {...user, isChecked:checked}} )
      this.setState({users:tempUser})
      let x = tempUser.map((item) => {
        return {
          Id: item.id,
        }
      })
  this.setState({selectedIds:x});
    }else{
    let tempUser = this.state.users.map(user=> user.id == name?{...user, isChecked : checked}: user)
    this.setState({users:tempUser})
    let a = tempUser.filter((user)=>user.isChecked)
    let x = a.map((item) => {
      return {
        Id: item.id,
      }
    })
  this.setState({selectedIds:x})
  }
    // const id = [a.id];
    // this.setState({selectedIds:this.state.selectedIds.concat(id)})
    // console.log(this.state.selectedIds)


// this.setState({checkBoxe.target.checked})
  }
  handleLabel=async()=>{
    if(this.state.selectedIds.length>0){
      const data = {
        url: "add_priority",
        body: {
          ids: this.state.selectedIds,
        },
      };
      const newData = await postMethod(data);
      this.getUsername()
     this.setState({selectedIds:[]})
      if (newData.status === 1) {
        // console.log("chatList", newData);
  
        // setUsers(newData.data.user_name_list);
      } else if (newData.status === false) {
        tokenError(newData.error);
      }
    }
  
  }
    handlePin=async()=>{
      if(this.state.selectedIds.length>0){
      const data = {
        url: "add_pin",
        body: {
          ids: this.state.selectedIds,
        },
      };
      const newData = await postMethod(data);
      this.getUsername()
     this.setState({selectedIds:[]})

     
      if (newData.status === 1) {
        // console.log("chatList", newData);
  
        // setUsers(newData.data.user_name_list);
      } else if (newData.status === false) {
        tokenError(newData.error);
      }
    }
  }
  handleUnRead=async()=>{
    if(this.state.selectedIds.length>0){
    const data = {
      url: "add_unread_chat ",
      body: {
        ids: this.state.selectedIds,
      },
    };
    
    const newData = await postMethod(data);
    this.getUsername()
    this.setState({selectedIds:[]})
   
    if (newData.status === 1) {
      // console.log("chatList", newData);

      // setUsers(newData.data.user_name_list);
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  }
}


  // sameId = (index, arr, user) => {
  //   if (index == arr.length - 1) {
  //     console.log("im called");
  //   }
  //   // return (
  //   //   <div className="chatCount ">
  //   //     {user.chat_historys_count > 0 ? user.chat_historys_count : null}
  //   //   </div>
  //   // );
  // };

  render() {
    const { users } = this.state;
    return (
      <div className="my-3 h-100">
        <Form.Group className="w-100 mb-0 chat-src px-3" controlId="email">
          <Form.Control
            type="search"
            name="search"
            // size="lg"
            ref={this.props.searchBoxRef}
            autoComplete="off"
            onChange={this.onInputChange}
            className="mb-4 bg_2"
            placeholder="Search User"
          />
        </Form.Group>
        <div className="w-100 d-flex pl-1">
        <Form.Check className="all_check_chat" name="allSelect" 
                    checked={users.filter(user=>user?.isChecked!==true).length<1}
                     onChange={
                      this.handleCheck
                    }
                    />
                    <img className="img_chat_box_drop" src={Down}/>
          <img className="img_chat_box" src={LabelImg} onClick={this.handleLabel}/>
          <img className="img_chat_box" src={PinImg} onClick={this.handlePin}/>
          <img className="img_chat_box" src={markUnRead} onClick={this.handleUnRead}/>
          <Form.Label className="chatbox_label">Sort By</Form.Label>
              <Form.Control
                as="select"
                name="program_id"
                // value={filterInputs.program_id}
                onChange={this.handleFilterInput}
                className="filter_chat_box bg_2"
              >
                <option value="">None</option>
                <option value="priority">Priority</option>
                <option value="unread">Unread</option>
              </Form.Control>


        </div>

        <ul className="chat-user-lists customScrollBar">
          {!this.state.userListLoading ? (
            users.length > 0 ? (
              users.map((user, index) => {
                return (
                  <>
                  <div className={user.is_priarity=="1"?"d-flex bg_pink border-bottom":"d-flex border-bottom"} >
                    <Form.Check className="check_chat" name={user.id} 
                    checked={user?.isChecked || false}
                     onChange={(e) => {
                      this.handleCheck(e, index,user);
                      
                    }}/>
                  <li
                    key={index}
                    onClick={(e) => {
                      this.linkClicked(e, user);
                    }}
                    
                    className="cursor pl-2 py-3 chat-user-list"
                  >
                  <label className="mb-0 text-right text-capitalize timeText">
                      {this.setTimeAgo(user)}
                    </label>
                    <div className="chat_list_box_1 ">
                      <img
                        src={user.image_url ? user.image_url : PlaceholderImage}
                        className="chat_img_box"
                      />
                      <div className="chat_name_box d-flex">
                      <h6 className="mb-0 ml-2 name_label_chat">{user.first_name}</h6>
                      {user.is_pinned?<img src={readPin} className="redPin"/>:null}
                      {user.is_priarity?<img className="redPin" src={redLabel}/>:null}
                      </div>
                      <div className="chat_icon_box">
                      {/* {user.is_unread=="1"?<img className="num_chat" src={Num}/>:null} */}
                      {this.state.count_loaded &&
                      user.id != this.props.id &&
                      user.chat_historys_count > 0 && (
                        <div className="redCircle">
                          {user.chat_historys_count > 99
                            ? "99+"
                            : user.chat_historys_count}
                        </div>
                      )}
                      </div>

                   
                    </div>
                    {/* {JSON.stringify(user.chat_historys_count, null, 2)} */}
                    {/* {this.state.count_loaded &&
                      user.id != this.props.id &&
                      user.chat_historys_count > 0 && (
                        <div className="chatCount ">
                          {user.chat_historys_count > 99
                            ? "99+"
                            : user.chat_historys_count}
                        </div>
                      )} */}
                  </li>
                  </div>
                  </>
                );
              })
            ) : (
              <li className="px-2 py-3">No users found</li>
            )
          ) : (
            <p className="px-2 py-3">Loading Users...</p>
          )}
        </ul>
      </div>
    );
  }
}
