import React, { useEffect } from "react";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter,
  useLocation,
} from "react-router-dom";
import { PrivateRoute } from "./Library";

// import { connect } from "react-redux";
// import { getUserData } from "./store/actions";

import Header from "./components/header";

import LoginPage from "./pages/login";
import ForgetpswdPage from "./pages/forgetpswd";
import ResetpswdPage from "./pages/resetpswd";
import NotfoundPage from "./pages/notfound";
import ManageUserProgress from "./pages/manageuserprogress";
import MealTracker from "./pages/mealtracker";
import UserChat from "./pages/userchat";

// import Userchat from "./pages/Userchat";

// import ViewDetails from "./pages/viewDetails";
import Waterintake from "./pages/Waterintake";
import Stepscount from "./pages/Stepcount";
import Workout from "./pages/Workout";
import Dietplan from "./pages/Dietplan";
import Processtracker from "./pages/Processtracker";
import Meal from "../src/pages/Meal"

// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

export default function App() {
  return (
    <>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={LoginPage} />
          <Route
            exact
            path="/forgot-password"
            component={withRouter(ForgetpswdPage)}
          />
          <Route
            exact
            path="/reset-password/:token"
            component={withRouter(ResetpswdPage)}
          />
          <Route
            exact
            path="/manage-user-progress"
            component={withRouter(ManageUserProgress)}
          />
          <Route
            exact
            path="/meal-tracker/"
            component={withRouter(MealTracker)}
          />
          <Route exact path="/user-chat/" component={withRouter(UserChat)} />
          {/* <Route exact path="/view-details" component={ViewDetails} />

          <Route exact path="/W" component={Waterintake}/>
          <Route exact path="/S" component={Stepscount}/>
          <Route exact path="/C" component={Workout}/>
          <Route exact path="/D" component={Dietplan}/>
          <Route exact path="/P" component={Processtracker}/>
          <Route exact path="/M" component={Meal}/> */}
          <Route path="*" component={NotfoundPage} />

          
        </Switch>
      </Router>

     


   
    </>
  );
}

// const mapStateToProps = (state) => {
//   console.log(state);
//   return {
//     user: state.user,
//   };
// };

// const mapDispatchToProps = {
//   updateUser: getUserData,
// };

// export default connect(mapStateToProps, mapDispatchToProps)(App);
