import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCloudUploadAlt } from "@fortawesome/free-solid-svg-icons";

export default function Index(props) {
  const [name, setName] = useState();

  const setNameFn = (e) => {
    if (e.target.files.length > 0) {
      setName(e.target.files[0].name);
      // console.log(e.target.files);
      props.data(e.target.files[0]);
    } else {
      props.data();
      setName("None");
    }
  };

  return (
    <>
      <div className="imgUploadBox d-flex align-items-center justify-content-center">
        {props.image_url && !name && (
          <img src={props.image_url} alt="program image" />
        )}
        <input
          type="file"
          className="w-100"
          id="avatar"
          name="avatar"
          accept="image/*"
          onChange={setNameFn}
        />
        <FontAwesomeIcon icon={faCloudUploadAlt} />
        {name && <small>Selected: {name}</small>}
      </div>
    </>
  );
}
