import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCloudUploadAlt } from "@fortawesome/free-solid-svg-icons";

import { postMethodFormData } from "../../Library";

export default function ImageBoxUpload(props) {
  const [imageUrl, setImageUrl] = useState();
  const [uploading, setUploading] = useState(false);
  const [err, setErr] = useState();

  const uploadImageFn = async (e) => {
    if (e.target.files.length > 0) {
      setErr();
      setUploading(true);
      let formDataBody = new FormData();
      formDataBody.append("image_url", e.target.files[0]);
      const data = {
        url: "image_upload",
        body: formDataBody,
      };
      const newData = await postMethodFormData(data);
      if (newData.status == 1) {
        let url = newData.data.image_url;
        if (url && newData.data) setImageUrl(url);
        props.data(url);
        setUploading(false);
        setErr();
      } else {
        setErr(newData.message);
      }
    }
  };

  useEffect(() => {
    if (props.image_url) {
      setImageUrl(props.image_url);
    }
  }, [props.image_url]);

  return (
    <>
      <div className="imgUploadBox d-flex align-items-center justify-content-center mwx-400 ml-auto mr-auto">
        {imageUrl && <img src={imageUrl} alt="image" />}
        <input
          type="file"
          className="w-100"
          id="avatar"
          name="avatar"
          accept="image/*"
          onChange={uploadImageFn}
        />
        <FontAwesomeIcon icon={faCloudUploadAlt} />
        {err && !uploading && <small className="err-feedback">{err}</small>}
        {err && !uploading && (
          <small className="err-feedback mt-4">This Field is required</small>
        )}
        {uploading && <small>Uploading Image...</small>}
      </div>
    </>
  );
}
