import React, { Component } from "react";
import { Button } from "react-bootstrap";

import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImages } from "@fortawesome/free-solid-svg-icons";

export default class GalleryBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
    };
  }

  render() {
    const { photoIndex, isOpen } = this.state;
    const images = this.props.images;
    const captions = this.props.captions;
    const position = this.props?.position ? this.props?.position : "na";

    return (
      <div>
        <div
          className={`d-flex align-items-center justify-content-${position}`}
        >
          <Button
            type="button"
            variant="link"
            onClick={() => this.setState({ isOpen: true })}
          >
            <u>
              <b>
                Open Image Gallery
                <span className="ml-2" style={{ verticalAlign: "top" }}>
                  <FontAwesomeIcon icon={faImages} />
                </span>
              </b>
            </u>
          </Button>
        </div>

        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length,
              })
            }
            imageTitle={captions?captions[photoIndex]:'Image-1'}
            imageLoadErrorMessage={"Oops!! Image not found or unable to load the image."}
          />
        )}
      </div>
    );
  }
}
