import React from "react";
import { Button, Modal } from "react-bootstrap";

export default function ModalPop(props) {
  const enteredModal = () => {
    // console.log("modal open");
  };
  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      onEntered={props.onEnter ? props.onEnter : enteredModal}
      size="xl"
      dialogClassName={props?.modalSize ? props.modalSize : "auto"}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <span className="text-capitalize">{props.modalhead}</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{props.modalcontent}</Modal.Body>
      {/* <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer> */}
    </Modal>
  );
}
