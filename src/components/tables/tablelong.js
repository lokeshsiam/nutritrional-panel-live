import React from "react";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import BootstrapTable from "react-bootstrap-table-next";

export default function TableLong(props) {
  const formatDatas = (initArr) => {
    let sn = 0;
    let filteredarray = [];
    initArr.forEach((element) => {
      filteredarray.push({
        ...element,
        sno: sn + 1,
        // action: element.id,
      });
      sn++;
    });
    return filteredarray;
  };

  const data = formatDatas(props.data);
  const columns = props.columns;

  return (
    <div className="customTable mt-5">
      <ToolkitProvider keyField="sno" data={data} columns={columns} search>
        {(props) => (
          <div>
            <BootstrapTable
              noDataIndication="No Results Found"
              bordered={false}
              {...props.baseProps}
              bootstrap4={true}
              headerWrapperClasses="thead-dark"
              bodyClasses="tableBody"
              wrapperClasses="table-responsive"
            />
          </div>
        )}
      </ToolkitProvider>
    </div>
  );
}
