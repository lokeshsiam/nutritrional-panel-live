// live
// export const localUrl = "https://api.nourishwithsim.org/api/";

// dev
export const localUrl = "https://nwsapi.underdev.in/api/";

// test
// export const localUrl = "https://testnwsapi.underdev.in/api/";

// pusher key test and dev
export const pusherKey = "d7d4a056535c53ef539e";

// pusher key live
// export const pusherKey = "a00ba68339c8d955e76f";
